/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stop_movement.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 13:05:54 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		stop_movement(int key, t_var *var)
{
	if (key == W_KEY)
		var->events.w_key = 0;
	else if (key == A_KEY)
		var->events.a_key = 0;
	else if (key == S_KEY)
		var->events.s_key = 0;
	else if (key == D_KEY)
		var->events.d_key = 0;
	else if (key == UP_KEY)
		var->events.up_key = 0;
	else if (key == RIGHT_KEY)
		var->events.right_key = 0;
	else if (key == DOWN_KEY)
		var->events.down_key = 0;
	else if (key == LEFT_KEY)
		var->events.left_key = 0;
	else if (key == SP_KEY)
		var->events.sp_key = 0;
	anim_enemy(var);
	return (0);
}
