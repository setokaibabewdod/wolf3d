/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemy.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 10:55:12 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void				move_to_player_2(t_var *var, t_point diff, t_point p,
		int i)
{
	if (diff.x > 0 && var->sprites[i].x - ENEMY_SPEED >= 0 &&
			!var->map[(int)p.y][(int)(p.x - ENEMY_SPEED)])
		var->sprites[i].x -= ENEMY_SPEED;
	else if (diff.x < 0 && var->sprites[i].x + ENEMY_SPEED <= var->map_x &&
			!var->map[(int)p.y][(int)(p.x + ENEMY_SPEED)])
		var->sprites[i].x += ENEMY_SPEED;
	else if (diff.x == 0)
		var->sprites[i].x = var->player->pos.x;
}

void					move_to_player(t_var *var, int i)
{
	t_point				diff;
	t_point				p;

	if (!var->sprites[i].moving)
		return ;
	p.x = var->sprites[i].x;
	p.y = var->sprites[i].y;
	diff.x = p.x - var->player->pos.x;
	diff.y = p.y - var->player->pos.y;
	if (check_enemy_on_map_x(var, diff.x, i) == 1)
		move_to_player_2(var, diff, p, i);
	if (check_enemy_on_map_y(var, diff.y, i) == 1)
	{
		if (diff.y > 0 && var->sprites[i].y - ENEMY_SPEED >= 0 &&
				!var->map[(int)(p.y - ENEMY_SPEED)][(int)p.x])
			var->sprites[i].y -= ENEMY_SPEED;
		else if (diff.y < 0 && var->sprites[i].y + ENEMY_SPEED <= var->map_y &&
				!var->map[(int)(p.y + ENEMY_SPEED)][(int)p.x])
			var->sprites[i].y += ENEMY_SPEED;
		else if (diff.y == 0)
			var->sprites[i].y = var->player->pos.y;
	}
}

static void				anim_enemy2(t_var *var, int i)
{
	if (var->sprites[i].base_tex == EN_NORMIE_DEAD ||
		var->sprites[i].base_tex == EN_MECH_DEAD ||
		var->sprites[i].base_tex == EN_PRIEST_DEAD)
		dead_enemy_texture(&var->sprites[i]);
	else
		(var->sprites[i].texture) = var->sprites[i].base_tex;
}

int						anim_enemy(t_var *var)
{
	int					i;
	int					anim_mod;

	i = -1;
	var->key_pressed++;
	if (var->key_pressed == 10)
	{
		while (++i < var->num_sprites)
		{
			if (var->sprites[i].is_enemy)
			{
				move_to_player(var, i);
				if (player_hit_by_melee(var, var->sprites[i]))
					var->gameover = 1;
				anim_mod = var->sprites[i].base_tex % var->sprites[i].nb_anim;
				if (!var->sprites[i].is_dead)
					(var->sprites[i].texture)++;
				if (((var->sprites[i].texture % var->sprites[i].nb_anim) ==
						anim_mod))
					anim_enemy2(var, i);
			}
		}
		var->key_pressed = 0;
	}
	return (1);
}
