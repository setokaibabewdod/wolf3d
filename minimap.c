/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 13:52:08 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	draw_editor_square(t_var *var, t_minimap_pack mp)
{
	int		square_x;
	int		square_y;

	square_x = mp.x / var->blocksize;
	square_y = mp.y / var->blocksize;
	if (square_x == var->mini_x && square_y == var->mini_y)
		var->img_str[mp.i] = 0xF00000;
}

void	draw_grid(t_var *var, t_minimap_pack mp)
{
	mp.x = ((mp.j + 1) % (var->map_x * var->blocksize));
	mp.y = ((mp.j + 1) / (var->map_x * var->blocksize));
	if (mp.x % var->blocksize == 0 || mp.y % var->blocksize == 0 ||
		mp.x == 1 || mp.j > ((var->map_y * var->blocksize) *
		(var->map_x * var->blocksize)) - ((var->map_x * var->blocksize)))
		var->img_str[mp.i] = 0x000000;
	mp.x = mp.j % (var->map_x * var->blocksize);
	mp.y = mp.j / (var->map_x * var->blocksize);
	if (var->map_editor == 2)
		draw_editor_square(var, mp);
}

void	draw_player(t_var *var, t_minimap_pack mp)
{
	int		size;

	if (!((var->blocksize / 2) % 2))
		size = (var->blocksize / 2) + 1;
	else
		size = var->blocksize / 2;
	if (size < 6)
		size = 6;
	if ((abs((mp.x - mp.px) * (mp.x - mp.px)) +
		abs((mp.y - mp.py) * (mp.y - mp.py))) <= size)
		var->img_str[mp.i] = 0xFF0000;
}

void	draw_minimap(t_var *var, t_minimap_pack mp)
{
	while (mp.i <= (LENGTH - var->off_mini) + LENGTH * (mp.my + var->off_mini))
	{
		mp.square_x = (((mp.i % LENGTH) - (LENGTH - var->off_mini - mp.mx))
						/ var->blocksize);
		mp.square_y = (((mp.i / LENGTH) - var->off_mini) / var->blocksize);
		if (mp.square_x < var->map_x && mp.square_y < var->map_y)
		{
			color_pix(var, mp);
			draw_grid(var, mp);
			draw_vision(var, mp);
			draw_player(var, mp);
			mp.j++;
		}
		if (mp.i % LENGTH < LENGTH - var->off_mini)
			mp.i++;
		else
			mp.i += LENGTH - mp.mx;
		if (mp.x == mp.mx)
		{
			mp.x = 0;
			mp.y++;
		}
		else
			mp.x++;
	}
}

void	minimap(t_var *var)
{
	t_minimap_pack	mp;

	mp.mx = var->blocksize * var->map_x;
	mp.my = var->blocksize * var->map_y;
	mp.range = (var->blocksize) * (var->blocksize) * 25;
	mp.j = 0;
	mp.i = (LENGTH - mp.mx - var->off_mini) + LENGTH * var->off_mini;
	mp.p = LENGTH - mp.mx - var->off_mini + (int)((var->player->pos.x *
			mp.mx) / var->map_x) + LENGTH * (var->off_mini +
			(int)((var->player->pos.x * mp.my) / var->map_y));
	mp.px = (var->player->pos.x / var->map_x) * mp.mx;
	mp.py = (var->player->pos.y / var->map_y) * mp.my;
	mp.x = 0;
	mp.y = 0;
	draw_minimap(var, mp);
}
