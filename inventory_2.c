/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inventory_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 10:35:50 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			draw_inventory_3(t_var *var, t_bmp *bmp_sprite,
		float ratio, int *offset_x)
{
	t_point_int		p;
	t_point_int		p_rel;
	int				color;

	p.x = -1;
	while (++p.x < (float)bmp_sprite->header.width_px * ratio)
	{
		p.y = -1;
		while (++p.y < (float)bmp_sprite->header.height_px * ratio)
		{
			p_rel.x = p.x + *offset_x;
			p_rel.y = (p.y + HEIGHT / 4 -
					(float)bmp_sprite->header.height_px * ratio) * LENGTH;
			color = get_color(bmp_sprite,
					(float)p.x / ratio, (float)p.y / ratio);
			if (get_color(bmp_sprite, (float)p.x / ratio,
						(float)p.y / ratio) != PINK)
				var->hud_str[p_rel.x + p_rel.y] = color;
		}
	}
}

static void			draw_inventory_2(t_var *var, int i,
		int j, int *offset_x)
{
	t_bmp			*bmp_sprite;
	float			ratio;

	if (var->player->inventory[i] != -1)
	{
		bmp_sprite = var->tab_sprite[var->player->inventory[i]];
		ratio = (float)var->side / (float)bmp_sprite->header.height_px;
		draw_inventory_3(var, bmp_sprite, ratio, offset_x);
		j++;
		(*offset_x) += (float)bmp_sprite->header.width_px * ratio + 10 * j;
	}
}

void				draw_inventory(t_var *var)
{
	int				i;
	int				j;
	int				offset_x;

	i = 0;
	j = 0;
	offset_x = 13;
	while (i < NUM_OBJ)
	{
		if (var->player->inventory[i] != -1)
			draw_inventory_2(var, i, j, &offset_x);
		i++;
	}
}

void				new_inventory(t_player *player)
{
	int				i;

	i = 0;
	while (i < NUM_OBJ)
	{
		player->inventory[i] = -1;
		i++;
	}
	player->inventory_empty = 1;
}
