/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 13:20:26 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	draw_cone(t_var *var, t_minimap_pack mp, t_angle_pack ap)
{
	if (!mp.invert && ap.angle <= ap.end && ap.angle >= ap.start)
		var->img_str[mp.i] += 0x101010;
	else if (mp.invert && ((ap.start <= ap.angle && ap.angle <= M_PI) ||
				(-M_PI <= ap.angle && ap.angle <= ap.end)))
		var->img_str[mp.i] += 0x101010;
	else if (mp.invert && mp.y == mp.py && mp.x <= mp.px)
		var->img_str[mp.i] += 0x101010;
}

void	draw_vision(t_var *var, t_minimap_pack mp)
{
	t_angle_pack	ap;

	mp.invert = 0;
	if ((abs((mp.x - mp.px) * (mp.x - mp.px)) +
				abs((mp.y - mp.py) * (mp.y - mp.py))) < mp.range)
	{
		ap.angle = atan2(mp.y - mp.py, mp.x - mp.px);
		ap.start = atan2(var->player->dir.y, var->player->dir.x);
		ap.end = ap.start;
		ap.start -= M_PI / 6;
		ap.end += M_PI / 6;
		if (ap.start < -M_PI)
		{
			mp.invert--;
			ap.start += 2 * M_PI;
		}
		if (ap.end > M_PI)
		{
			mp.invert++;
			ap.end -= 2 * M_PI;
		}
		draw_cone(var, mp, ap);
	}
}

void	color_pix(t_var *var, t_minimap_pack mp)
{
	mp.x = (((mp.i % LENGTH) - (LENGTH - var->off_mini -
					(var->map_x * var->blocksize))) / var->blocksize);
	mp.y = (((mp.i / LENGTH) - var->off_mini) / var->blocksize);
	if (!(var->map[mp.y][mp.x]))
		var->img_str[mp.i] = 0x5C5C5C;
	else if (var->map[mp.y][mp.x] == 1)
		var->img_str[mp.i] = 0x202020;
	else if (var->map[mp.y][mp.x] / 10 == 1)
		var->img_str[mp.i] = 0x202020;
	else if (var->map[mp.y][mp.x] / 10 == 2)
		var->img_str[mp.i] = 0x402020;
	else if (var->map[mp.y][mp.x] / 10 == 3)
		var->img_str[mp.i] = 0x204020;
	else if (var->map[mp.y][mp.x] / 10 == 4)
		var->img_str[mp.i] = 0x202040;
	else if (var->map[mp.y][mp.x] / 10 == 5)
		var->img_str[mp.i] = 0x404020;
	else if (var->map[mp.y][mp.x] / 10 == 6)
		var->img_str[mp.i] = 0x204040;
	else if (var->map[mp.y][mp.x] / 10 == 7)
		var->img_str[mp.i] = 0x00FF00;
	else if (var->map[mp.y][mp.x] / 10 == 8)
		var->img_str[mp.i] = 0x800020;
	else
		var->img_str[mp.i] = 0x202080;
}
