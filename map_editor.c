/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_editor.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 11:32:18 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		fill_line(char **line, t_var *var, int y)
{
	int		x;

	x = 0;
	while (x < var->map_x)
	{
		if (!(*line = ft_strjoin_free(*line, ft_itoa(var->map[y][x]), 1)))
			ft_error(var);
		if (!(*line = ft_strjoin_free(*line, " ", 1)))
			ft_error(var);
		x++;
	}
	if (!(*line = ft_strjoin_free(*line, "\n", 1)))
		ft_error(var);
}

void		save_map(t_var *var)
{
	int		y;
	char	*name;
	char	*line;
	int		fd;

	y = 0;
	name = "map";
	ft_file_name_gen(&name);
	if ((fd = open(name, O_CREAT | O_RDWR, 0644)) < 0)
		ft_error(var);
	if (!(line = ft_strnew(0)))
		ft_error(var);
	while (y < var->map_y)
	{
		fill_line(&line, var, y);
		y++;
	}
	ft_putstr_fd(line, fd);
	ft_putstr("Saved in : ");
	ft_putendl(name);
	close(fd);
}

void		fillmap(t_var *var)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while (y < var->map_y)
	{
		while (x < var->map_x)
		{
			var->map[y][x] = 0;
			x++;
		}
		x = 0;
		y++;
	}
}

static void	map_editor2(t_var *var, int x, int y)
{
	int		i;

	i = 0;
	while (i < var->map_y)
	{
		free(var->map[i]);
		i++;
	}
	free(var->map);
	if (!(var->map = (int **)malloc(sizeof(int *) * y)))
		ft_error(var);
	i = 0;
	while (i < y)
	{
		if (!(var->map[i] = (int *)malloc(sizeof(int) * x)))
			ft_error(var);
		i++;
	}
	var->map_x = x;
	var->map_y = y;
}

void		map_editor(t_var *var)
{
	int		x;
	int		y;
	int		i;
	char	*line;

	i = 0;
	if (var->map_editor == 1)
	{
		ft_putstr("Please choose map's size (i.e.: 4x5, max : 102x102)\n");
		if ((get_next_line(0, &line) < 0))
			ft_error(var);
		if ((x = ft_atoi(line)) > 102 || x <= 0)
			ft_error(var);
		while (line[i] && line[i] >= '0' && line[i] <= '9')
			i++;
		i++;
		if ((y = ft_atoi(&line[i])) > 102 || y <= 0)
			ft_error(var);
		if (x != var->map_x || y != var->map_y)
			map_editor2(var, x, y);
		var->blocksize = (x > y) ? var->map_size / x : var->map_size / y;
		fillmap(var);
		var->map_editor = 2;
	}
}
