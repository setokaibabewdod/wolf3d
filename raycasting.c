/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 12:24:43 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_bmp				*select_bmp(t_raycast_pack rp, t_var *var)
{
	int				n;
	t_bmp			*bmp;

	bmp = NULL;
	if (rp.perp_wall_dist < 30)
	{
		n = var->map[rp.map_y][rp.map_x];
		if (rp.side == 1)
			n++;
		if (n != 0 && !(bmp = var->tab_bmp[n]))
			return (NULL);
	}
	else
	{
		if (!(bmp = var->tab_bmp[0]))
			return (NULL);
	}
	return (bmp);
}

void				vertical_line(t_raycast_pack rp, t_var *var)
{
	int				y;
	int				init;
	int				off_height;
	t_bmp			*bmp;

	y = 0;
	init = rp.draw_start;
	bmp = select_bmp(rp, var);
	if (var->fog_col)
	{
		rp.draw_start = HEIGHT / 2 - var->fog_col / 2;
		rp.draw_end = HEIGHT / 2 + var->fog_col / 2;
	}
	while (rp.x >= 0 && rp.x < var->img->max && y++ < rp.draw_start)
		rp.x += LENGTH;
	while (rp.x >= 0 && rp.x < var->img->max && rp.draw_start < rp.draw_end)
	{
		off_height = (rp.line_height > HEIGHT) ?
				rp.line_height / 2 - HEIGHT / 2 : 0;
		var->img_str[rp.x] = get_color(bmp, (rp.wall_x * bmp->header.height_px),
				((((float)off_height + (float)rp.draw_start - (float)init)) /
				((float)rp.line_height)) * bmp->header.height_px);
		rp.draw_start++;
		rp.x += LENGTH;
	}
}

static void			ray_cast3(t_var *var, t_raycast_pack *rp)
{
	if (rp->ray_dir_x < 0)
	{
		rp->step_x = -1;
		rp->side_dist_x = (var->player->pos.x - rp->map_x) * rp->delta_dist_x;
	}
	else
	{
		rp->step_x = 1;
		rp->side_dist_x = (rp->map_x + 1.0 - var->player->pos.x)
			* rp->delta_dist_x;
	}
	if (rp->ray_dir_y < 0)
	{
		rp->step_y = -1;
		rp->side_dist_y = (var->player->pos.y - rp->map_y) * rp->delta_dist_y;
	}
	else
	{
		rp->step_y = 1;
		rp->side_dist_y = (rp->map_y + 1.0 - var->player->pos.y)
			* rp->delta_dist_y;
	}
}

static void			ray_cast2(t_var *var, t_raycast_pack *rp, double *z_buffer)
{
	rp->perp_wall_dist = (rp->side) ? ((rp->map_y - var->player->pos.y +
			(1 - rp->step_y) / 2) / rp->ray_dir_y) :
					((rp->map_x - var->player->pos.x +
						(1 - rp->step_x) / 2) / rp->ray_dir_x);
	rp->line_height = (rp->perp_wall_dist) ?
			(int)(HEIGHT / rp->perp_wall_dist) : HEIGHT;
	rp->draw_start = -rp->line_height / 2 + HEIGHT / 2;
	if (rp->draw_start < 0)
		rp->draw_start = 0;
	rp->draw_end = rp->line_height / 2 + HEIGHT / 2;
	if (rp->draw_end >= HEIGHT)
		rp->draw_end = HEIGHT - 1;
	rp->wall_x = (!rp->side) ?
			(var->player->pos.y + rp->perp_wall_dist * rp->ray_dir_y) :
				var->player->pos.x + rp->perp_wall_dist * rp->ray_dir_x;
	rp->wall_x -= floor((rp->wall_x));
	draw_floor_sky(var, *rp);
	if (rp->hit == 1)
		vertical_line(*rp, var);
	z_buffer[rp->x] = rp->perp_wall_dist;
}

void				ray_cast(t_var *var)
{
	t_raycast_pack	rp;
	double			z_buffer[LENGTH];

	rp.x = 0;
	while (rp.x < LENGTH)
	{
		rp = new_raycast_pack(var, rp);
		ray_cast3(var, &rp);
		ray_cast_dda(var, &rp);
		draw_skybox(var, rp.x, rp.hit);
		ray_cast2(var, &rp, z_buffer);
		rp.x++;
	}
	draw_sprites(var, z_buffer);
	draw_weapon(var, var->player->weapon_anim);
	if (!(var->player->inventory_empty))
		draw_inventory(var);
}
