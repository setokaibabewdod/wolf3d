/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hooks1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 13:39:02 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	map_editor_movement1(int key, t_var *var)
{
	if (key == UP_KEY && var->map_editor == 2 && var->mini_y > 0)
	{
		if (var->lock == 0)
			var->mini_y--;
		else
		{
			var->mini_y--;
			var->map[var->mini_y][var->mini_x] = var->lock_val;
		}
	}
	else if (key == DOWN_KEY && var->map_editor == 2 &&
			var->mini_y < var->map_y - 1)
	{
		if (var->lock == 0)
			var->mini_y++;
		else
		{
			var->mini_y++;
			var->map[var->mini_y][var->mini_x] = var->lock_val;
		}
	}
}

static void	map_editor_movement2(int key, t_var *var)
{
	if (key == LEFT_KEY && var->map_editor == 2 && var->mini_x > 0)
	{
		if (var->lock == 0)
			var->mini_x--;
		else
		{
			var->mini_x--;
			var->map[var->mini_y][var->mini_x] = var->lock_val;
		}
	}
	else if (key == RIGHT_KEY && var->map_editor == 2 &&
			var->mini_x < var->map_x - 1)
	{
		if (var->lock == 0)
			var->mini_x++;
		else
		{
			var->mini_x++;
			var->map[var->mini_y][var->mini_x] = var->lock_val;
		}
	}
}

static void	speed_and_map(int key, t_var *var)
{
	if (key == NKMN_KEY && var->map_editor != 2 && var->p_speed > 0.2)
		var->p_speed -= 0.1;
	else if (key == NKMN_KEY && var->map_editor == 2)
		var->map[var->mini_y][var->mini_x] = 0;
	else if (key == M_KEY)
	{
		if (var->map_bool == 0)
		{
			var->map_bool = 1;
			if (var->map_x > var->map_y)
				var->blocksize = (LENGTH - 2 * var->off_mini) / var->map_x;
			else
				var->blocksize = (HEIGHT - 2 * var->off_mini) / var->map_y;
		}
		else
		{
			var->map_bool = 0;
			if (var->map_x > var->map_y)
				var->blocksize = var->map_size / var->map_x;
			else
				var->blocksize = var->map_size / var->map_y;
		}
	}
}

static void	speed_and_map_editor(int key, t_var *var)
{
	if (key == NKPL_KEY && var->p_speed < 1)
		var->p_speed += 0.1;
	else if (key == G_KEY)
	{
		save_map(var);
		var->map_editor = 0;
	}
	else if (key == H_KEY && var->map_editor == 0)
		var->map_editor = 2;
}

void		key_hooks1(int key, t_var *var)
{
	map_editor_movement1(key, var);
	map_editor_movement2(key, var);
	speed_and_map(key, var);
	speed_and_map_editor(key, var);
}
