/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 14:39:58 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_color			new_color(void)
{
	t_color		color;

	color.off_r = 0;
	color.off_g = 0;
	color.off_b = 0;
	return (color);
}

t_var			*new_var(char *map)
{
	t_var		*var;

	(void)map;
	if (!(var = (t_var*)ft_memalloc(sizeof(t_var))))
		ft_error(NULL);
	if (!(var->player = new_player(var)))
		ft_error(NULL);
	var->map_editor = 0;
	init_tab_bmp(var);
	if (check_tab_bmp(var) == -1)
		ft_error(NULL);
	init_tab_sprite(var);
	if (check_tab_sprite(var) == -1)
		ft_error(NULL);
	init_tab_enemies(var);
	init_anim(var);
	var->anim_started = 0;
	var->key_pressed = 0;
	var->gameover = 0;
	return (var);
}

static int		init_img_2(t_var *var)
{
	var->fog_col = 0;
	var->map_editor = 0;
	var->mini_x = 0;
	var->mini_y = 0;
	if (var->map_x > var->map_y)
		var->blocksize = var->map_size / var->map_x;
	else
		var->blocksize = var->map_size / var->map_y;
	if (var->blocksize == 0)
		var->blocksize = 1;
	if (check_pos(var) == 0)
		return (ft_error(NULL));
	return (1);
}

int				init_img(t_var *var)
{
	var->img = (t_img *)malloc(sizeof(t_img));
	var->img->endian = ENDIAN;
	var->img->bits = BITS_PER_PIXEL;
	var->img->height = HEIGHT;
	var->img->length = LENGTH;
	var->img->max = LENGTH * HEIGHT;
	var->player->dir.x = 1;
	var->player->dir.y = 0;
	var->player->old_dir.x = 1;
	var->player->old_dir.y = 0;
	var->player->plane.x = 0;
	var->player->plane.y = 0.60;
	var->map_bool = 0;
	var->p_speed = 0.1;
	var->map_size = 250;
	var->off_mini = 20;
	var->draw_hud = 1;
	return (init_img_2(var));
}
