/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_hud.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 15:56:15 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int		check_two(t_var *var, int x, int y, int off_y)
{
	if (((x <= LENGTH - 13 && x >= 3 * LENGTH / 4 + 11) ||
			(x >= LENGTH / 2 + 11 && x <= 3 * LENGTH / 4 - 11) ||
			(x >= 13 && x <= 13 + var->side) ||
			(x >= var->side + 13 + 1 * 10 &&
			x <= 2 * var->side + 13 + 1 * 10) ||
			(x >= 2 * var->side + 13 + 2 * 10 &&
			x <= 3 * var->side + 13 + 2 * 10) ||
			(x >= 3 * var->side + 13 + 3 * 10 &&
			x <= 4 * var->side + 13 + 3 * 10) ||
			(x >= 4 * var->side + 13 + 4 * 10 &&
			x <= 5 * var->side + 13 + 4 * 10)) &&
			(y >= off_y - 11 && y <= HEIGHT / 3 - 11 -
			(2 * (HEIGHT / 3 - var->side) / 7)))
		return (1);
	return (0);
}

static int		check_one(t_var *var, int x, int y, int off_y)
{
	if (x <= 2 || x >= LENGTH - 3 || y <= 2 || y >= HEIGHT / 3 - 3 ||
		(x >= LENGTH / 2 - 1 && x <= LENGTH / 2 + 1) ||
		(x >= 3 * LENGTH / 4 - 1 && x <= 3 * LENGTH / 4 + 1) ||
		(((x <= LENGTH - 13 && x >= 3 * LENGTH / 4 + 11) ||
		(x >= LENGTH / 2 + 11 && x <= 3 * LENGTH / 4 - 11) ||
		(x >= 13 && x <= 13 + var->side) || (x >= var->side + 13 + 1 * 10 &&
		x <= 2 * var->side + 23) ||
		(x >= 2 * var->side + 33 && x <= 3 * var->side + 33) ||
		(x >= 3 * var->side + 43 && x <= 4 * var->side + 43) ||
		(x >= 4 * var->side + 53 && x <= 5 * var->side + 53)) &&
		(y == off_y - 11 || y == HEIGHT / 3 - 11 -
		(2 * (HEIGHT / 3 - var->side) / 7))) ||
		((x == 13 || x == 13 + var->side || x == var->side + 23 ||
		x == 2 * var->side + 13 + 1 * 10 || x == 2 * var->side + 13 + 20 ||
		x == 3 * var->side + 33 || x == 3 * var->side + 13 + 30 ||
		x == 4 * var->side + 43 || x == 4 * var->side + 13 + 40 ||
		x == 5 * var->side + 53 || x == LENGTH - 13 ||
		x == 3 * LENGTH / 4 + 11 || x == LENGTH / 2 + 11 ||
		x == 3 * LENGTH / 4 - 11) &&
			(y >= off_y - 11 && y <= HEIGHT / 3 - 11 -
			(2 * (HEIGHT / 3 - var->side) / 7))))
		return (1);
	return (0);
}

static void		init_hud(t_var *var)
{
	if (var->hud_ptr)
		mlx_destroy_image(var->mlx_ptr, var->hud_ptr);
	if (!(var->hud_ptr = mlx_new_image(var->mlx_ptr, LENGTH, HEIGHT / 3)))
		ft_error(var);
	var->hud_str = (int *)mlx_get_data_addr(var->hud_ptr, &(var->img->bits),
			&(var->img->length), &(var->img->endian));
}

void			draw_hud(t_var *var)
{
	int			i;
	int			x;
	int			y;
	int			max;
	int			off_y;

	init_hud(var);
	i = -1;
	var->side = (LENGTH / 2 - 5 * 10) / 5 - 3;
	off_y = (5 * (HEIGHT / 3 - var->side) / 7);
	max = var->img->max / 3;
	while (++i < max)
	{
		x = i % LENGTH;
		y = i / LENGTH;
		if (check_one(var, x, y, off_y))
			var->hud_str[i] = 0x000000;
		else if (check_two(var, x, y, off_y))
			var->hud_str[i] = 0x685C76;
		else
			var->hud_str[i] = 0x46404E;
	}
}
