/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   life.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 11:42:00 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				enemy_is_dead(t_sprite *sprite)
{
	if (sprite->life <= 0)
		return (1);
	return (0);
}

int				player_is_dead(t_player *player)
{
	if (player->life <= 0)
		return (1);
	return (0);
}

int				player_hit_by_melee(t_var *var, t_sprite sprite)
{
	if (!sprite.is_dead &&
			(int)sprite.x == (int)var->player->pos.x &&
			(int)sprite.y == (int)var->player->pos.y)
		var->player->life -= sprite.attack;
	return (player_is_dead(var->player));
}
