/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_events.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 12:22:56 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	manage_events4(t_var *var)
{
	if (var->events.s_key == 1 || ((var->map_editor == 0 ||
	var->map_editor == 3) && var->events.down_key == 1))
	{
		if (var->player->pos.x - var->player->dir.x * var->p_speed < var->map_x
			&& var->player->pos.x - var->player->dir.x * var->p_speed >= 0 &&
			var->map[(int)var->player->pos.y][(int)(var->player->pos.x -
			var->player->dir.x * var->p_speed)] == 0)
			var->player->pos.x -= var->player->dir.x * var->p_speed;
		if (var->player->pos.y - var->player->dir.y * var->p_speed < var->map_y
			&& var->player->pos.y - (var->player->dir.y * var->p_speed) >= 0 &&
			!var->map[(int)(var->player->pos.y - var->player->dir.y *
			var->p_speed)][(int)var->player->pos.x])
			var->player->pos.y -= var->player->dir.y * var->p_speed;
	}
}

static void	manage_events3(t_var *var)
{
	int			i;
	int			j;

	i = var->map[(int)var->player->pos.y]
			[(int)(var->player->pos.x - var->player->dir.y * var->p_speed)];
	j = var->map[(int)(var->player->pos.y + var->player->dir.x *
				var->p_speed)][(int)var->player->pos.x];
	if (var->events.d_key == 1 || ((var->map_editor == 0 ||
			var->map_editor == 3) && var->events.right_key == 1))
	{
		if (var->player->pos.x - (var->player->dir.y * var->p_speed) <
				var->map_x && var->player->pos.x - (var->player->dir.y *
				var->p_speed) >= 0 && !i)
			var->player->pos.x -= var->player->dir.y * var->p_speed;
		if (var->player->pos.y + var->player->dir.x * var->p_speed < var->map_y
		&& var->player->pos.y + var->player->dir.x * var->p_speed >= 0 && !j)
			var->player->pos.y -= -var->player->dir.x * var->p_speed;
	}
	if (var->events.sp_key == 1)
		var->anim_started = 1;
}

static void	manage_events2(t_var *var)
{
	if (var->events.a_key == 1 || ((var->map_editor == 0 ||
			var->map_editor == 3) && var->events.left_key == 1))
	{
		if (var->player->pos.x + var->player->dir.y * var->p_speed < var->map_x
			&& var->player->pos.x + var->player->dir.y * var->p_speed >= 0 &&
			var->map[(int)var->player->pos.y][(int)(var->player->pos.x +
			var->player->dir.y * var->p_speed)] == 0)
			var->player->pos.x += var->player->dir.y * var->p_speed;
		if (var->player->pos.y - var->player->dir.x * var->p_speed < var->map_y
			&& var->player->pos.y - var->player->dir.x * var->p_speed >= 0 &&
			!var->map[(int)(var->player->pos.y - var->player->dir.x *
			var->p_speed)][(int)var->player->pos.x])
			var->player->pos.y += -var->player->dir.x * var->p_speed;
	}
	manage_events4(var);
	manage_events3(var);
}

int			manage_events(t_var *var)
{
	if (var->events.w_key == 1 || ((var->map_editor == 0 ||
			var->map_editor == 3) && var->events.up_key == 1))
	{
		if (var->player->pos.x + var->player->dir.x * var->p_speed < var->map_x
			&& var->player->pos.x + var->player->dir.x * var->p_speed >= 0 &&
			var->map[(int)var->player->pos.y][(int)(var->player->pos.x +
			var->player->dir.x * var->p_speed)] == 0)
			var->player->pos.x += var->player->dir.x * var->p_speed;
		if (var->player->pos.y + var->player->dir.y * var->p_speed < var->map_y
			&& var->player->pos.y + var->player->dir.y * var->p_speed >= 0 &&
			var->map[(int)(var->player->pos.y + var->player->dir.y *
			var->p_speed)][(int)var->player->pos.x] == 0)
			var->player->pos.y += var->player->dir.y * var->p_speed;
	}
	manage_events2(var);
	return (0);
}
