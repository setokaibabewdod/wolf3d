/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 14:17:19 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:46:06 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H
# include <fcntl.h>
# include <stdlib.h>
# include <math.h>
# include "Libft/libft.h"
# include "keycode.h"
# include "bmp.h"
# include "minilibx_macos/mlx.h"
# include <stdio.h>

# define WHITE			0xFFFFFF
# define RED			0x8B0000
# define BLACK			0x000000
# define GREY			0x696969
# define OPP_PINK		0x67FF77
# define PINK			0x980088

# define WIN_L			1700
# define WIN_H			1024
# define ENDIAN			1
# define LENGTH 		1280
# define HEIGHT 		768
# define BITS_PER_PIXEL	8

# define ROTSPEED		0.05

# define NUM_ENEMY		37
# define NUM_OBJ		5
# define NUM_WEAPON		4
# define DIFF_BMP		87
# define DIFF_SPRITE	86
# define DIFF_ANIM		20
# define DIFF_ENEMY		37

# define BARREL			0
# define KEY_BASIC_DOOR	1
# define KEY_END_DOOR	2

# define ZOOM_WEAPON	8
# define ZOOM_INVENT	2
# define PLAYER_LIFE_TOTAL	50
# define ENEMY_SPEED	0.15

typedef struct			s_angle_pack
{
	float				angle;
	float				start;
	float				end;
}						t_angle_pack;

typedef struct			s_minimap_pack
{
	int					i;
	int					j;
	int					p;
	int					px;
	int					py;
	int					x;
	int					y;
	int					mx;
	int					my;
	int					range;
	int					square_x;
	int					square_y;
	char				invert;
}						t_minimap_pack;

typedef struct			s_floor_pack
{
	float				floor_x_wall;
	float				floor_y_wall;
	float				dist_wall;
	float				dist_player;
	float				current_dist;
	float				weight;
	float				current_floor_x;
	float				current_floor_y;
	int					floor_tex_x;
	int					floor_tex_y;
	int					off_height;
}						t_floor_pack;

typedef struct			s_raycast_pack
{
	float				ray_dir_x;
	float				ray_dir_y;
	int					map_x;
	int					map_y;
	float				side_dist_x;
	float				side_dist_y;
	float				delta_dist_x;
	float				delta_dist_y;
	int					step_x;
	int					step_y;
	int					hit;
	int					side;
	float				perp_wall_dist;
	int					x;
	int					line_height;
	int					draw_start;
	int					draw_end;
	float				wall_x;
}						t_raycast_pack;

typedef struct			s_sprite_pack
{
	float				sprite_x;
	float				sprite_y;
	float				transform_x;
	float				transform_y;
	int					sprite_screen_x;
	int					sprite_height;
	int					draw_start_y;
	int					draw_end_x;
	int					draw_end_y;
	int					sprite_width;
	int					stripe;
	int					i;
	int					tex_x;
}						t_sprite_pack;

typedef struct			s_points
{
	int					x0;
	int					y0;
	int					x1;
	int					y1;
	int					dx;
	int					sx;
	int					dy;
	int					sy;
	int					err;
	int					e2;
}						t_points;

typedef struct			s_event
{
	int					a_key;
	int					w_key;
	int					s_key;
	int					d_key;
	int					up_key;
	int					down_key;
	int					left_key;
	int					right_key;
	int					sp_key;
}						t_event;

typedef struct			s_order
{
	int					order;
	float				distance;
}						t_order;

typedef struct			s_sprite
{
	double				x;
	double				y;
	int					texture;
	int					base_tex;
	int					order;
	int					distance;
	int					lootable;
	int					is_enemy;
	int					nb_anim;
	int					life;
	int					attack;
	char				moving;
	char				is_dead;
}						t_sprite;

typedef struct			s_bmp_header
{
	uint16_t			type;
	uint16_t			size_1;
	uint16_t			size_2;
	uint16_t			reserved1;
	uint16_t			reserved2;
	uint16_t			offset1;
	uint16_t			offset2;
	uint16_t			dib_header_size1;
	uint16_t			dib_header_size2;
	int16_t				width_px1;
	int16_t				width_px2;
	int16_t				height_px1;
	int16_t				height_px2;
	uint16_t			num_planes;
	uint16_t			bits_per_pixel;
	uint16_t			compression1;
	uint16_t			compression2;
	uint16_t			image_size_bytes1;
	uint16_t			image_size_bytes2;
	int16_t				x_resolution_ppm1;
	int16_t				x_resolution_ppm2;
	int16_t				y_resolution_ppm1;
	int16_t				y_resolution_ppm2;
	uint16_t			num_colors1;
	uint16_t			num_colors2;
	uint16_t			important_colors1;
	uint16_t			important_colors2;
}						t_bmp_header;

typedef struct			s_bmp_header_f
{
	int					type;
	int					size;
	uint16_t			reserved1;
	uint16_t			reserved2;
	int					offset;
	int					dib_header_size;
	int					width_px;
	int					height_px;
	int					num_planes;
	int					bits_per_pixel;
	int					compression;
	int					image_size_bytes;
	int					x_res;
	int					y_res;
	int					num_colors;
	int					important_colors;
	int					padding;
}						t_bmp_header_f;

typedef struct			s_bmp
{
	t_bmp_header_f		header;
	int					*data;
	int					nb;
}						t_bmp;

typedef struct			s_color
{
	int					off_r;
	int					off_b;
	int					off_g;
}						t_color;

typedef struct			s_img
{
	int					endian;
	int					bits;
	int					height;
	int					length;
	int					max;
}						t_img;

typedef struct			s_point
{
	float				x;
	float				y;
}						t_point;

typedef struct			s_point_int
{
	int					x;
	int					y;
}						t_point_int;

typedef struct			s_segment
{
	t_point				p1;
	t_point				p2;
}						t_segment;
typedef struct			s_enemy
{
	t_point				pos;
	int					life;
	int					attack;
	int					texture;
}						t_enemy;

typedef struct			s_player
{
	t_point				pos;
	t_point				dir;
	t_point				prev_dir;
	t_point				plane;
	t_point				old_dir;
	int					inventory[NUM_OBJ];
	int					weapons[NUM_WEAPON];
	int					inventory_empty;
	int					weapon_anim;
	int					life;
	int					life_total;
	char				is_shooting;
}						t_player;

typedef struct			s_var
{
	void				*mlx_ptr;
	void				*win_ptr;
	void				*img_ptr;
	void				*hud_ptr;
	void				*go_ptr;
	int					*img_str;
	int					*hud_str;
	int					*go_str;
	int					height;
	int					length;
	float				off_x;
	float				off_y;
	t_img				*img;
	float				zoom;
	int					zoom_s;
	int					max_i;
	float				x1;
	float				x2;
	float				y1;
	float				y2;
	int					map_x;
	int					map_y;
	int					size;
	char				**tmp_map;
	int					**map;
	t_player			*player;
	float				p_speed;
	int					blocksize;
	int					map_size;
	int					off_mini;
	int					map_bool;
	int					temp_i;
	int					fog_col;
	int					map_editor;
	int					mini_x;
	int					mini_y;
	int					lock;
	int					lock_val;
	int					num_sprites;
	t_bmp				*tab_bmp[DIFF_BMP];
	t_bmp				*tab_sprite[DIFF_SPRITE];
	t_bmp				*tab_anim[DIFF_ANIM];
	t_bmp				*tab_enemy[DIFF_ENEMY];
	t_sprite			*sprites;
	t_sprite			sprite_enemy[DIFF_ENEMY];
	int					anim_started;
	t_enemy				*enemies[NUM_ENEMY];
	int					side;
	int					draw_hud;
	int					key_pressed;
	int					gameover;
	t_event				events;
}						t_var;

/*
** animation.c
*/

int						anim_hook(int key, t_var *var);
int						anim_weapon(t_var *var);
void					init_anim(t_var *var);
void					draw_weapon(t_var *var, int i);

/*
** bmp.c
*/

int						get_color(t_bmp *bmp, int x, int y);
int						check_tab_bmp(t_var *var);
int						check_tab_sprite(t_var *var);
void					init_tab_sprite(t_var *var);
int						read_color_vertical(t_bmp *bmp, int x, int y,
						unsigned char *tmp);
int						*extract_from_char(char *tab, t_bmp *bmp);
int						convert_to_32(uint16_t left, uint16_t right);
t_bmp_header_f			convert_to_header(t_bmp_header src);
t_bmp					*read_img_bmp(char *file, t_bmp_header_f header);
t_bmp					*read_header_bmp(char *file);

/*
** countwords.c
*/

int						countwords(char **temp);

/*
** dda.c
*/

void					ray_cast_dda(t_var *var, t_raycast_pack *rp);
t_raycast_pack			new_raycast_pack(t_var *var, t_raycast_pack rp);

/*
** double_swap.c
*/

int						double_swap(t_order *tab_order, int i, int j);

/*
** draw_gameover.c
*/

void					draw_gameover(t_var *var);

/*
** draw_hud.c
*/

void					draw_hud(t_var *var);

/*
** enemy.c
*/

int						check_enemy_on_map_x(t_var *var, float dx, int i);
int						check_enemy_on_map_y(t_var *var, float dy, int i);
void					move_to_player(t_var *var, int i);
int						anim_enemy(t_var *var);

/*
** error.c
*/

void					*free_bmp(t_bmp *bmp, int *data, unsigned char *tmp);
void					ft_freeall(t_var *var);
int						ft_error(t_var *var);
int						error_sprites(t_sprite *sprites, t_order *tab_order);
int						ft_usage(void);

/*
** floor.c
*/

void					draw_floor_sky(t_var *var, t_raycast_pack rp);

/*
** ft_file_name_gen.c
*/

int						ft_file_name_gen(char **name);

/*
** free_tabs.c
*/

void					free_tabs(t_var *var);

/*
** init_struct.c
*/

t_bmp					*new_bmp(char *str);
t_var					*new_var();
t_color					new_color();
int						init_img(t_var *var);

/*
** init_tab_bmp.c
*/

void					init_tab_bmp(t_var *var);

/*
** inventory.c
*/

void					draw_inventory(t_var *var);
void					new_inventory(t_player *player);
int						player_has_obj(t_player *player, int object);
int						add_item_to_inventory(t_player *player, int object);
int						del_item_from_inventory(t_player *player, int object);

/*
** key_hooks1.c
*/

void					key_hooks1(int key, t_var *var);

/*
** key_hooks2.c
*/

void					key_hooks2(int key, t_var *var);

/*
** legend.c
*/

void					gr_display_frame(t_var *var, t_point p2d,
						t_point size_frame, int color);
void					gr_layout(t_var *var);

/*
** life.c
*/

int						player_hit_by_melee(t_var *var, t_sprite sprite);

/*
** manage_events.c
*/

void					save_map(t_var *var);
int						manage_events(t_var *var);

/*
** map_editor.c
*/

void					map_editor(t_var *var);

/*
** minimap.c
*/

void					minimap(t_var *var);

/*
** minimap2.c
*/

void					draw_vision(t_var *var, t_minimap_pack mp);
void					color_pix(t_var *var, t_minimap_pack mp);

/*
** mlx_event.c
*/

int						quit_prog(int key, void *var);
int						ft_key_hook(int key, t_var *var);
int						ft_expose_hook(t_var *var);
int						rotation(int x, int y, t_var *var);

/*
** mouse_event.c
*/

int						mouse_event(int x, int y, t_var *var);

/*
** object.c
*/

int						check_object(t_var *var);
void					remove_object(t_var *var);

/*
** player.c
*/

t_player				*new_player(t_var *var);
int						check_pos(t_var *var);

/*
** point.c
*/

t_point					new_point(float x, float y);
t_point_int				new_int_point(int x, int y);

/*
** raycasting.c
*/

t_bmp					*select_bmp(t_raycast_pack rp, t_var *var);
void					vertical_line(t_raycast_pack rp, t_var *var);
void					ray_cast(t_var *var);

/*
** read.c
*/

int						ft_maplength(char *src, t_var *var);
int						**ft_convmap(char **src, t_var *var);
int						ft_readmap(char *src, t_var *var);

/*
** shoot.c
*/

void					shoot_on_enemy(t_sprite *enemy);
void					dead_enemy_texture(t_sprite *enemy);

/*
** skybox.c
*/

void					draw_skybox(t_var *var, int x, int hit);

/*
** sprite_generator.c
*/

t_sprite				new_sprite(double x, double y, int texture,
						int lootable);
int						check_empty_place(t_var *var, t_point point);
void					init_sprites(t_var *var);

/*
** sprite_info.c
*/

void					get_sprite_info(t_var *var, char *map);

/*
** sprites.c
*/

int						draw_sprites(t_var *var, double *z_buffer);

/*
** stop_movement.c
*/

int						stop_movement(int key, t_var *var);

/*
** tab_sprite.c
*/

void					init_tab_sprite(t_var *var);
void					init_tab_enemies(t_var *var);

#endif
