/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 13:52:08 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		main(int argc, char **argv)
{
	t_var	*var;

	if (argc != 2)
		return (ft_usage());
	if (!(var = new_var(argv[1])))
		return (ft_error(NULL));
	if (ft_readmap(argv[1], var) || var->map_x > 102 || var->map_y > 102)
		return (ft_error(var));
	get_sprite_info(var, argv[1]);
	if (!(var->mlx_ptr = mlx_init()))
		return (ft_error(var));
	if (!(var->win_ptr = mlx_new_window(var->mlx_ptr, WIN_L, WIN_H, "WOLF3D")))
		return (ft_error(var));
	init_img(var);
	gr_layout(var);
	mlx_mouse_hide();
	mlx_hook(var->win_ptr, KP, NOEVENTMASK, ft_key_hook, var);
	mlx_hook(var->win_ptr, MOTIONNOTIFY, POINTERMOTIONMASK, mouse_event, var);
	mlx_hook(var->win_ptr, DESTROYNOTIFY, NOEVENTMASK, quit_prog, var);
	mlx_hook(var->win_ptr, KEYRELEASE, KEYRELEASEMASK, stop_movement, var);
	mlx_loop_hook(var->mlx_ptr, anim_weapon, var);
	mlx_mouse_move(var->win_ptr, WIN_L / 2, WIN_H / 2);
	ft_expose_hook(var);
	mlx_loop(var->mlx_ptr);
	return (0);
}
