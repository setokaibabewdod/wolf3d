/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   animation.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 16:21:03 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				anim_weapon(t_var *var)
{
	if (var->anim_started == 1)
	{
		var->events.sp_key = 2;
		var->player->weapon_anim++;
		if (var->player->weapon_anim % 5 == 0)
		{
			var->player->weapon_anim -= 5;
			var->anim_started = 0;
		}
		var->player->is_shooting = (var->player->weapon_anim % 5 == 2) ? 1 : 0;
		draw_weapon(var, var->player->weapon_anim);
		var->events.sp_key = 0;
	}
	if (!var->gameover)
		anim_enemy(var);
	manage_events(var);
	ft_expose_hook(var);
	return (0);
}

void			init_anim(t_var *var)
{
	var->tab_anim[0] = new_bmp(ANIM_0_0);
	var->tab_anim[1] = new_bmp(ANIM_0_1);
	var->tab_anim[2] = new_bmp(ANIM_0_2);
	var->tab_anim[3] = new_bmp(ANIM_0_3);
	var->tab_anim[4] = new_bmp(ANIM_0_4);
	var->tab_anim[5] = new_bmp(ANIM_1_0);
	var->tab_anim[6] = new_bmp(ANIM_1_1);
	var->tab_anim[7] = new_bmp(ANIM_1_2);
	var->tab_anim[8] = new_bmp(ANIM_1_3);
	var->tab_anim[9] = new_bmp(ANIM_1_4);
	var->tab_anim[10] = new_bmp(ANIM_2_0);
	var->tab_anim[11] = new_bmp(ANIM_2_1);
	var->tab_anim[12] = new_bmp(ANIM_2_2);
	var->tab_anim[13] = new_bmp(ANIM_2_3);
	var->tab_anim[14] = new_bmp(ANIM_2_4);
	var->tab_anim[15] = new_bmp(ANIM_3_0);
	var->tab_anim[16] = new_bmp(ANIM_3_1);
	var->tab_anim[17] = new_bmp(ANIM_3_2);
	var->tab_anim[18] = new_bmp(ANIM_3_3);
	var->tab_anim[19] = new_bmp(ANIM_3_4);
}

void			draw_weapon(t_var *var, int i)
{
	t_bmp		*bmp_sprite;
	t_point_int	p2d;
	int			color;
	t_point_int	rel_p2d;

	bmp_sprite = var->tab_anim[i];
	p2d.x = 0;
	while (p2d.x < bmp_sprite->header.width_px * ZOOM_WEAPON)
	{
		p2d.y = 0;
		while (p2d.y < bmp_sprite->header.height_px * ZOOM_WEAPON)
		{
			rel_p2d.x = p2d.x + LENGTH / 2 -
				bmp_sprite->header.width_px * ZOOM_WEAPON / 2;
			rel_p2d.y = (p2d.y + HEIGHT -
					bmp_sprite->header.height_px * ZOOM_WEAPON) * LENGTH;
			color = get_color(bmp_sprite, p2d.x / ZOOM_WEAPON,
					p2d.y / ZOOM_WEAPON);
			if (color != PINK)
				var->img_str[rel_p2d.x + rel_p2d.y] = color;
			p2d.y++;
		}
		p2d.x++;
	}
}
