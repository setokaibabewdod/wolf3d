# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/11 12:21:01 by jreynaer          #+#    #+#              #
#    Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

SRC = *.c

OBJ = $(SRC:.c=.o)

CFLAGS = -Wall -Wextra -Werror

LIBFT = ./Libft/libft.a

FRAMEWORK = -framework OpenGL -framework Appkit

$(NAME): $(OBJ)
	@make -C ./Libft/
	@echo 'Libft compilation : OK'
	@gcc $(CFLAGS) -o $(NAME) $(SRC) \
		$(LIBFT) -L minilibx_macos/ -lmlx $(FRAMEWORK)
	@echo 'wolf3d compilation : OK'

all: $(NAME)

%.o: %.c wolf3d.h
	gcc $(CFLAGS) -c -o $@ $<

clean:
	@rm -f $(OBJ)
	@echo 'wolf3d clean : OK'
	@make clean -C ./Libft/
	@echo 'Libft clean : OK'

fclean: clean
	@rm -f $(NAME)
	@make fclean -C ./Libft/

re: fclean all

.PHONY: all clean fclean re
