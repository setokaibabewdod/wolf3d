/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_generator.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 13:11:14 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_sprite		new_sprite(double x, double y, int texture, int lootable)
{
	t_sprite	sprite;

	sprite.x = x;
	sprite.y = y;
	sprite.texture = texture;
	sprite.order = 0;
	sprite.distance = 0;
	sprite.lootable = lootable;
	sprite.life = 5;
	sprite.attack = 5;
	return (sprite);
}

int				check_empty_place(t_var *var, t_point point)
{
	if (var->map[(int)point.x + (int)(point.y * var->map_x)] != 0)
		return (0);
	return (1);
}
