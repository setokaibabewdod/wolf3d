/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 13:36:32 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_maplength(char *src, t_var *var)
{
	int		i;
	int		c;

	i = 0;
	c = 0;
	while (src[i])
	{
		if ((src[i] >= '0' && src[i] <= '9') || src[i] == '-')
		{
			c++;
			while (src[i] && ((src[i] >= '0' && src[i] <= '9') ||
					src[i] == '-'))
				i++;
		}
		else if (src[i] == ' ')
			i++;
		else
			ft_error(var);
	}
	return (c);
}

void	check_n_fill(int *i, t_var *var, char **tmp)
{
	if ((var->map[i[0]][i[1]] = ft_atoi(tmp[i[1]])) < 0 ||
		(var->map[i[0]][i[1]] = ft_atoi(tmp[i[1]])) > DIFF_BMP - 1)
		ft_error(var);
}

int		**ft_convmap(char **src, t_var *var)
{
	char	**tmp;
	int		i[2];
	int		first;

	i[0] = -1;
	i[1] = -1;
	if ((first = ft_maplength(src[0], var)) == 0)
		ft_error(var);
	if (!(var->map = (int **)malloc(sizeof(int *) * var->map_y)))
		ft_error(var);
	var->map_x = first;
	while (++i[0] < var->map_y)
	{
		if (ft_maplength(src[i[0]], var) != first)
			ft_error(var);
		if (!(var->map[i[0]] = (int *)malloc(sizeof(int) * var->map_x)))
			ft_error(var);
		if (!(tmp = ft_strsplit(src[i[0]], ' ')))
			ft_error(var);
		while (++i[1] < var->map_x)
			check_n_fill(i, var, tmp);
		i[1] = -1;
		free(tmp);
	}
	return (var->map);
}

void	check_n_close(t_var *var, int fd, char ***dest, char *temp)
{
	if (var->map_y == 0)
		ft_error(var);
	if ((close(fd)) < 0)
		ft_error(var);
	if (!(*dest = ft_strsplit(temp, '\n')))
		ft_error(var);
}

int		ft_readmap(char *src, t_var *var)
{
	int		fd;
	char	*temp;
	char	*line;
	char	**dest;

	var->map_y = 0;
	if (var->map_editor == 0 && ((fd = open(src, O_RDONLY)) < 0))
		ft_error(var);
	else if (var->map_editor != 0 && (fd = open(src, O_RDWR)) < 0)
		ft_error(var);
	if (!(temp = ft_strnew(0)))
		ft_error(var);
	while ((get_next_line(fd, &line)) > 0)
	{
		if (var->map_y == 0 && ft_strlen(line) == 0)
			ft_error(var);
		var->map_y++;
		if (!(temp = ft_strjoin_free(temp, line, 3)))
			ft_error(var);
		if (!(temp = ft_strjoin_free(temp, "\n", 1)))
			ft_error(var);
	}
	check_n_close(var, fd, &dest, temp);
	ft_convmap(dest, var);
	return (0);
}
