/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/20 10:03:07 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	*free_bmp(t_bmp *bmp, int *data, unsigned char *tmp)
{
	if (tmp)
		free(tmp);
	if (data)
		free(data);
	if (bmp)
		free(bmp);
	return (NULL);
}

void	ft_freeall(t_var *var)
{
	if (var->win_ptr)
	{
		mlx_destroy_window(var->mlx_ptr, var->win_ptr);
		var->win_ptr = NULL;
	}
	if (var->img_ptr)
	{
		mlx_destroy_image(var->mlx_ptr, var->img_ptr);
		var->img_ptr = NULL;
	}
	if (var->mlx_ptr)
		ft_memdel((void **)&var->mlx_ptr);
	free_tabs(var);
	if (var)
	{
		if (var->img)
			ft_memdel((void **)&var->img);
		ft_memdel((void **)&var);
	}
}

int		error_sprites(t_sprite *sprites, t_order *tab_order)
{
	if (sprites)
		free(sprites);
	if (tab_order)
		free(tab_order);
	ft_putstr_fd("ERROR DURING LOADING SPRITES\n", 1);
	return (-1);
}

int		ft_error(t_var *var)
{
	(void)var;
	ft_putstr_fd("error\n", 1);
	exit(-1);
	return (-1);
}

int		ft_usage(void)
{
	ft_putstr_fd("usage:  ./wolf3d mapfile\n", 1);
	return (0);
}
