/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tab_bmp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 10:42:01 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			init_tab_bmp_4(t_var *var)
{
	var->tab_bmp[69] = new_bmp(BMP_69);
	var->tab_bmp[70] = new_bmp(BMP_70);
	var->tab_bmp[71] = new_bmp(BMP_71);
	var->tab_bmp[72] = new_bmp(BMP_72);
	var->tab_bmp[73] = new_bmp(BMP_73);
	var->tab_bmp[74] = new_bmp(BMP_74);
	var->tab_bmp[75] = new_bmp(BMP_75);
	var->tab_bmp[76] = new_bmp(BMP_76);
	var->tab_bmp[77] = new_bmp(BMP_77);
	var->tab_bmp[78] = new_bmp(BMP_78);
	var->tab_bmp[79] = new_bmp(BMP_79);
	var->tab_bmp[80] = new_bmp(BMP_80);
	var->tab_bmp[81] = new_bmp(BMP_81);
	var->tab_bmp[82] = new_bmp(BMP_82);
	var->tab_bmp[83] = new_bmp(BMP_83);
	var->tab_bmp[84] = new_bmp("bmp/doom_skybox.bmp");
	var->tab_bmp[85] = new_bmp("bmp/wolf3d.bmp");
	var->tab_bmp[86] = new_bmp("bmp/wolf3d2.bmp");
}

static void			init_tab_bmp_3(t_var *var)
{
	var->tab_bmp[46] = new_bmp(BMP_46);
	var->tab_bmp[47] = new_bmp(BMP_47);
	var->tab_bmp[48] = new_bmp(BMP_48);
	var->tab_bmp[49] = new_bmp(BMP_49);
	var->tab_bmp[50] = new_bmp(BMP_50);
	var->tab_bmp[51] = new_bmp(BMP_51);
	var->tab_bmp[52] = new_bmp(BMP_52);
	var->tab_bmp[53] = new_bmp(BMP_53);
	var->tab_bmp[54] = new_bmp(BMP_54);
	var->tab_bmp[55] = new_bmp(BMP_55);
	var->tab_bmp[56] = new_bmp(BMP_56);
	var->tab_bmp[57] = new_bmp(BMP_57);
	var->tab_bmp[58] = new_bmp(BMP_58);
	var->tab_bmp[59] = new_bmp(BMP_59);
	var->tab_bmp[60] = new_bmp(BMP_60);
	var->tab_bmp[61] = new_bmp(BMP_61);
	var->tab_bmp[62] = new_bmp(BMP_62);
	var->tab_bmp[63] = new_bmp(BMP_63);
	var->tab_bmp[64] = new_bmp(BMP_64);
	var->tab_bmp[65] = new_bmp(BMP_65);
	var->tab_bmp[66] = new_bmp(BMP_66);
	var->tab_bmp[67] = new_bmp(BMP_67);
	var->tab_bmp[68] = new_bmp(BMP_68);
	init_tab_bmp_4(var);
}

static void			init_tab_bmp_2(t_var *var)
{
	var->tab_bmp[23] = new_bmp(BMP_23);
	var->tab_bmp[24] = new_bmp(BMP_24);
	var->tab_bmp[25] = new_bmp(BMP_25);
	var->tab_bmp[26] = new_bmp(BMP_26);
	var->tab_bmp[27] = new_bmp(BMP_27);
	var->tab_bmp[28] = new_bmp(BMP_28);
	var->tab_bmp[29] = new_bmp(BMP_29);
	var->tab_bmp[30] = new_bmp(BMP_30);
	var->tab_bmp[31] = new_bmp(BMP_31);
	var->tab_bmp[32] = new_bmp(BMP_32);
	var->tab_bmp[33] = new_bmp(BMP_33);
	var->tab_bmp[34] = new_bmp(BMP_34);
	var->tab_bmp[35] = new_bmp(BMP_35);
	var->tab_bmp[36] = new_bmp(BMP_36);
	var->tab_bmp[37] = new_bmp(BMP_37);
	var->tab_bmp[38] = new_bmp(BMP_38);
	var->tab_bmp[39] = new_bmp(BMP_39);
	var->tab_bmp[40] = new_bmp(BMP_40);
	var->tab_bmp[41] = new_bmp(BMP_41);
	var->tab_bmp[42] = new_bmp(BMP_42);
	var->tab_bmp[43] = new_bmp(BMP_43);
	var->tab_bmp[44] = new_bmp(BMP_44);
	var->tab_bmp[45] = new_bmp(BMP_45);
	init_tab_bmp_3(var);
}

void				init_tab_bmp(t_var *var)
{
	var->tab_bmp[0] = new_bmp(BMP_0);
	var->tab_bmp[1] = new_bmp(BMP_1);
	var->tab_bmp[2] = new_bmp(BMP_2);
	var->tab_bmp[3] = new_bmp(BMP_3);
	var->tab_bmp[4] = new_bmp(BMP_4);
	var->tab_bmp[5] = new_bmp(BMP_5);
	var->tab_bmp[6] = new_bmp(BMP_6);
	var->tab_bmp[7] = new_bmp(BMP_7);
	var->tab_bmp[8] = new_bmp(BMP_8);
	var->tab_bmp[9] = new_bmp(BMP_9);
	var->tab_bmp[10] = new_bmp(BMP_10);
	var->tab_bmp[11] = new_bmp(BMP_11);
	var->tab_bmp[12] = new_bmp(BMP_12);
	var->tab_bmp[13] = new_bmp(BMP_13);
	var->tab_bmp[14] = new_bmp(BMP_14);
	var->tab_bmp[15] = new_bmp(BMP_15);
	var->tab_bmp[16] = new_bmp(BMP_16);
	var->tab_bmp[17] = new_bmp(BMP_17);
	var->tab_bmp[18] = new_bmp(BMP_18);
	var->tab_bmp[19] = new_bmp(BMP_19);
	var->tab_bmp[20] = new_bmp(BMP_20);
	var->tab_bmp[21] = new_bmp(BMP_21);
	var->tab_bmp[22] = new_bmp(BMP_22);
	init_tab_bmp_2(var);
}
