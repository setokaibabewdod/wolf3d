/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/20 10:04:28 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int					quit_prog(int key, void *var)
{
	(void)key;
	ft_freeall(var);
	exit(0);
}

static void			player_movement1(int key, t_var *var)
{
	if (key == SP_KEY && var->events.sp_key != 2)
		var->events.sp_key = 1;
	if (key == W_KEY)
		var->events.w_key = 1;
	else if (key == A_KEY)
		var->events.a_key = 1;
}

static void			player_movement2(int key, t_var *var)
{
	if (key == S_KEY)
		var->events.s_key = 1;
	else if (key == D_KEY)
		var->events.d_key = 1;
}

static void			miscellaneous(int key, t_var *var)
{
	if (key == ESC_KEY)
	{
		ft_freeall(var);
		exit(0);
	}
	else if (key == F5_KEY)
		init_img(var);
}

int					ft_key_hook(int key, t_var *var)
{
	int				i;

	player_movement1(key, var);
	player_movement2(key, var);
	key_hooks1(key, var);
	key_hooks2(key, var);
	miscellaneous(key, var);
	if (key == E_KEY)
	{
		if ((i = check_object(var)) != -1 &&
				add_item_to_inventory(var->player, i) == 1)
			remove_object(var);
	}
	else if (key == K1_KEY && var->player->weapons[0] != -1)
		var->player->weapon_anim = 15;
	else if (key == K2_KEY && var->player->weapons[1] != -1)
		var->player->weapon_anim = 10;
	else if (key == K3_KEY && var->player->weapons[2] != -1)
		var->player->weapon_anim = 5;
	else if (key == K4_KEY && var->player->weapons[3] != -1)
		var->player->weapon_anim = 0;
	return (0);
}
