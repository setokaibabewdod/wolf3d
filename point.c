/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 10:43:11 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_point				new_point(float x, float y)
{
	t_point			p2d;

	p2d.x = x;
	p2d.y = y;
	return (p2d);
}

t_point_int			new_point_int(int x, int y)
{
	t_point_int		p2d;

	p2d.x = x;
	p2d.y = y;
	return (p2d);
}
