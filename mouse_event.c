/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 14:08:26 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		mouse_event2(t_var *var, int *x)
{
	mlx_mouse_move(var->win_ptr, WIN_L / 2, WIN_H / 2);
	if (*x < 0)
	{
		*x += (WIN_L / 2);
		var->player->old_dir.x += (WIN_L / 2);
	}
	else if (*x > WIN_L)
	{
		*x -= (WIN_L / 2);
		var->player->old_dir.x -= (WIN_L / 2);
	}
}

int				mouse_event(int x, int y, t_var *var)
{
	double	rot_speed;
	double	old_dir_x;
	double	old_plane_x;

	rot_speed = 0;
	if (x < 0 || x > WIN_L || y < 0 || y > WIN_H)
		mouse_event2(var, &x);
	else
		rot_speed = ((2 * M_PI) * (x - var->player->old_dir.x)) / WIN_L;
	var->player->old_dir.x = x;
	old_dir_x = var->player->dir.x;
	var->player->dir.x = var->player->dir.x * cos(rot_speed) -
			var->player->dir.y * sin(rot_speed);
	var->player->dir.y = old_dir_x * sin(rot_speed) +
			var->player->dir.y * cos(rot_speed);
	old_plane_x = var->player->plane.x;
	var->player->plane.x = var->player->plane.x * cos(rot_speed) -
			var->player->plane.y * sin(rot_speed);
	var->player->plane.y = old_plane_x * sin(rot_speed) +
			var->player->plane.y * cos(rot_speed);
	return (0);
}
