/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <cvan-duf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 11:11:00 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			ray_cast_dda2(t_raycast_pack *rp)
{
	rp->side_dist_x += rp->delta_dist_x;
	rp->map_x += rp->step_x;
	rp->side = 0;
}

static void			ray_cast_dda3(t_raycast_pack *rp)
{
	rp->side_dist_y += rp->delta_dist_y;
	rp->map_y += rp->step_y;
	rp->side = 1;
}

static void			ray_cast_dda4(t_raycast_pack *rp)
{
	rp->side_dist_x += rp->delta_dist_x;
	rp->map_x += rp->step_x;
	rp->side = 0;
}

void				ray_cast_dda(t_var *var, t_raycast_pack *rp)
{
	while (rp->hit == 0)
	{
		if (rp->side_dist_x < rp->side_dist_y && (rp->map_x + rp->step_x) <
				var->map_x && (rp->map_x + rp->step_x) >= 0)
			ray_cast_dda2(rp);
		else if (rp->side_dist_x > rp->side_dist_y && (rp->map_y + rp->step_y) <
				var->map_y && (rp->map_y + rp->step_y) >= 0)
			ray_cast_dda3(rp);
		else
		{
			if (rp->side_dist_x < rp->side_dist_y)
				ray_cast_dda4(rp);
			else
			{
				rp->side_dist_y += rp->delta_dist_y;
				rp->map_y += rp->step_y;
				rp->side = 1;
			}
			break ;
		}
		if (var->map[rp->map_y][rp->map_x] > 0)
			rp->hit = 1;
	}
}

t_raycast_pack		new_raycast_pack(t_var *var, t_raycast_pack rp)
{
	rp.ray_dir_x = var->player->dir.x +
			var->player->plane.x * (2 * rp.x / (float)LENGTH - 1);
	rp.ray_dir_y = var->player->dir.y +
			var->player->plane.y * (2 * rp.x / (float)LENGTH - 1);
	rp.map_x = (int)var->player->pos.x;
	rp.map_y = (int)var->player->pos.y;
	rp.delta_dist_x = fabs(1 / rp.ray_dir_x);
	rp.delta_dist_y = fabs(1 / rp.ray_dir_y);
	rp.hit = 0;
	return (rp);
}
