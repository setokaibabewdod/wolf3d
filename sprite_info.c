/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_info.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 12:12:46 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:44:52 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		fill_sprites3(t_var *var, int i)
{
	if ((var->sprites[i].is_enemy && (!var->sprites[i].nb_anim ||
		var->sprites[i].base_tex + var->sprites[i].nb_anim >= DIFF_SPRITE)) ||
		var->sprites[i].x > var->map_x || var->sprites[i].y > var->map_y ||
		var->sprites[i].texture >= DIFF_SPRITE || var->sprites[i].nb_anim > 8)
		ft_error(var);
}

static void		fill_sprites2(t_var *var, char **temp, int i)
{
	int			j;

	if ((j = countwords(temp)) != 6)
		ft_error(var);
	while (--j)
	{
		if (j == 5)
			var->sprites[i].nb_anim = abs(ft_atoi(temp[j]));
		if (j == 4)
			var->sprites[i].is_enemy = abs(ft_atoi(temp[j]));
		if (j == 3)
			var->sprites[i].lootable = abs(ft_atoi(temp[j]));
		else if (j == 2)
		{
			var->sprites[i].texture = abs(ft_atoi(temp[j]));
			var->sprites[i].base_tex = var->sprites[i].texture;
		}
		else if (j == 1)
			var->sprites[i].y = (double)abs(ft_atoi(temp[j])) + 0.5;
		free(temp[j]);
	}
	if (j == 0)
		var->sprites[i].x = (double)abs(ft_atoi(temp[j])) + 0.5;
	fill_sprites3(var, i);
	free(temp[j]);
}

static void		fill_sprites(t_var *var, char **temp, int i)
{
	fill_sprites2(var, temp, i);
	var->sprites[i].attack = 5;
	var->sprites[i].life = 5;
	var->sprites[i].moving = (var->sprites[i].is_enemy) ? 1 : 0;
	var->sprites[i].is_dead = 0;
	free(temp);
}

void			get_sprite_info2(t_var *var, char *name)
{
	int			fd;
	int			error;
	int			i;
	char		*line;
	char		**temp;

	error = 0;
	i = 0;
	if (!(fd = open(name, O_RDONLY)))
		ft_error(var);
	while ((error = get_next_line(fd, &line)) > 0)
	{
		if (error < 0)
			ft_error(var);
		if (!(temp = ft_strsplit(line, ' ')))
			ft_error(var);
		free(line);
		fill_sprites(var, temp, i);
		i++;
	}
	var->num_sprites = i;
	if ((close(fd)) < 0)
		ft_error(var);
}

void			get_sprite_info(t_var *var, char *map)
{
	int			fd;
	int			i;
	char		*name;
	char		*line;
	int			error;

	i = 0;
	line = NULL;
	if (!(name = ft_strjoin(map, ".obj")))
		ft_error(var);
	if (!(fd = open(name, O_RDONLY)))
		ft_error(var);
	while ((error = get_next_line(fd, &line)) > 0)
	{
		free(line);
		i++;
	}
	if ((close(fd)) < 0)
		ft_error(var);
	if (!(var->sprites = malloc(sizeof(t_sprite) * i)))
		ft_error(var);
	get_sprite_info2(var, name);
}
