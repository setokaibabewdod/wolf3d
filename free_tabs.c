/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tabs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <cvan-duf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/23 11:00:30 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			free_tabs(t_var *var)
{
	int			i;

	i = -1;
	while (++i < DIFF_BMP)
	{
		if (var->tab_bmp[i])
		{
			free(var->tab_bmp[i]->data);
			free(var->tab_bmp[i]);
		}
	}
	i = -1;
	while (++i < DIFF_SPRITE)
	{
		if (var->tab_sprite[i])
		{
			free(var->tab_sprite[i]->data);
			free(var->tab_sprite[i]);
		}
	}
}
