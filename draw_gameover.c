/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_gameover.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 13:05:46 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	draw_gameover(t_var *var)
{
	int		x;
	int		y;

	y = 0;
	if (var->go_str)
		mlx_destroy_image(var->mlx_ptr, var->go_ptr);
	if (!(var->go_ptr = mlx_new_image(var->mlx_ptr, LENGTH, HEIGHT)))
		ft_error(var);
	var->go_str = (int *)mlx_get_data_addr(var->go_ptr, &(var->img->bits),
			&(var->img->length), &(var->img->endian));
	while (y < HEIGHT)
	{
		x = 0;
		while (x < LENGTH)
		{
			var->go_str[x + y * LENGTH] =
				get_color(var->tab_bmp[85],
						(int)(((float)x * 2.0 * ((128.0 * 4.0) / LENGTH))),
						(int)(((float)y * 2.0 * ((128.0 * 4.0) / HEIGHT))));
			x++;
		}
		y++;
	}
}
