/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   skybox.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/23 17:15:43 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			skybox_loop(t_var *var, int x, int end, t_point_int *tex)
{
	int				y;
	int				index;

	y = -1;
	while (++y < end)
	{
		tex->y = ((y * (HEIGHT)) / (2 * var->tab_bmp[84]->header.height_px));
		index = x + y * LENGTH;
		var->img_str[index] = get_color(var->tab_bmp[84], tex->x, tex->y);
	}
}

void				draw_skybox(t_var *var, int x, int hit)
{
	t_point_int		tex;
	float			angle;
	t_point_int		off;
	int				end;

	if (hit == 1)
		end = HEIGHT / 2;
	else
		end = HEIGHT;
	if (var->player->dir.x < -1)
		var->player->dir.x = -1;
	if (var->player->dir.x > 1)
		var->player->dir.x = 1;
	angle = acos(var->player->dir.x);
	if ((var->player->dir.x < 0 && var->player->dir.y < 0) ||
			var->player->dir.y < 0)
		angle *= -1;
	off.x = ((angle * (2 * var->tab_bmp[84]->header.width_px)) / (2 * M_PI));
	tex.x = ((x * LENGTH) / (4 * var->tab_bmp[84]->header.width_px)) + off.x;
	if (tex.x < 0)
		tex.x += var->tab_bmp[84]->header.width_px;
	skybox_loop(var, x, end, &tex);
}
