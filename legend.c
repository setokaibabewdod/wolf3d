/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   legend.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 10:34:54 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:46 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		gr_display_frame(t_var *var, t_point p2d,
		t_point size_frame, int color)
{
	int		i;
	int		j;

	i = p2d.x;
	while (i < size_frame.x)
	{
		j = p2d.y;
		while (j < size_frame.y)
			mlx_pixel_put(var->mlx_ptr, var->win_ptr,
					i, j++, color);
		i++;
	}
}

static void	gr_layout_bis(t_var *var, int i)
{
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "G : SAVE MAPFILE");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "H : EDIT CURRENT MAP");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "C : LOCK VALUE");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "ARROW KEYS : MOVE");
}

void		gr_layout(t_var *var)
{
	int		i;

	i = 10;
	gr_display_frame(var, new_point(0, 0), new_point(480, WIN_H), GREY);
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "WOLF3D");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "DESTROY THE NAZIS");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 50), RED, "W : MOVE FORWARD");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "S : MOVE BACKWARD");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "A : MOVE LEFT");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "D : MOVE RIGHT");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "M : MAP");
	mlx_string_put(var->mlx_ptr, var->win_ptr, 10,
			(i += 20), RED, "E : LOOT ITEM");
	gr_layout_bis(var, i);
}
