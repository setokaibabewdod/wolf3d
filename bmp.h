/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 12:58:32 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BMP_H
# define BMP_H

# define EN_NORMIE      70
# define EN_NORMIE_DEAD 77
# define EN_PRIEST      48
# define EN_PRIEST_DEAD 52
# define EN_MECH        59
# define EN_MECH_DEAD   66

# define BMP_0			"bmp/fog.bmp"
# define BMP_1			"bmp/armycross.bmp"
# define BMP_2			"bmp/armycross_dark.bmp"
# define BMP_3			"bmp/blueblank.bmp"
# define BMP_4			"bmp/blueblank_dark.bmp"
# define BMP_5			"bmp/bluedoor.bmp"
# define BMP_6			"bmp/bluedoor_dark.bmp"
# define BMP_7			"bmp/bluelift.bmp"
# define BMP_8			"bmp/bluelift_dark.bmp"
# define BMP_9			"bmp/bluemetal.bmp"
# define BMP_10			"bmp/bluemetal_dark.bmp"
# define BMP_11			"bmp/blueprison1.bmp"
# define BMP_12			"bmp/blueprison1_dark.bmp"
# define BMP_13			"bmp/blueprison2.bmp"
# define BMP_14			"bmp/blueprison2_dark.bmp"
# define BMP_15			"bmp/blueskull.bmp"
# define BMP_16			"bmp/blueskull_dark.bmp"
# define BMP_17			"bmp/bluestone2.bmp"
# define BMP_18			"bmp/bluestone2_dark.bmp"
# define BMP_19			"bmp/bluestone.bmp"
# define BMP_20			"bmp/bluestone_dark.bmp"
# define BMP_21			"bmp/blueswatiska.bmp"
# define BMP_22			"bmp/blueswatiska_dark.bmp"
# define BMP_23			"bmp/blueverboten.bmp"
# define BMP_24			"bmp/blueverboten_dark.bmp"
# define BMP_25			"bmp/bricks.bmp"
# define BMP_26			"bmp/bricks_dark.bmp"
# define BMP_27			"bmp/brownstone1.bmp"
# define BMP_28			"bmp/brownstone1_dark.bmp"
# define BMP_29			"bmp/brownstone2.bmp"
# define BMP_30			"bmp/brownstone2_dark.bmp"
# define BMP_31			"bmp/eagle1.bmp"
# define BMP_32			"bmp/eagle1_dark.bmp"
# define BMP_33			"bmp/eagle2.bmp"
# define BMP_34			"bmp/eagle2.bmp"
# define BMP_35			"bmp/eagle3.bmp"
# define BMP_36			"bmp/eagle3_dark.bmp"
# define BMP_37			"bmp/flag1.bmp"
# define BMP_38			"bmp/flag1_dark.bmp"
# define BMP_39			"bmp/glassadolf.bmp"
# define BMP_40			"bmp/glassadolf_dark.bmp"
# define BMP_41			"bmp/greybrick1.bmp"
# define BMP_42			"bmp/greybrick1_dark.bmp"
# define BMP_43			"bmp/greybrick2.bmp"
# define BMP_44			"bmp/greybrick2_dark.bmp"
# define BMP_45			"bmp/greybrick3.bmp"
# define BMP_46			"bmp/greybrick3_dark.bmp"
# define BMP_47			"bmp/greybrickadolf.bmp"
# define BMP_48			"bmp/greybrickadolf_dark.bmp"
# define BMP_49			"bmp/greystone1.bmp"
# define BMP_50			"bmp/greystone1_dark.bmp"
# define BMP_51			"bmp/greystone2.bmp"
# define BMP_52			"bmp/greystone2_dark.bmp"
# define BMP_53			"bmp/greystone3.bmp"
# define BMP_54			"bmp/greystone3_dark.bmp"
# define BMP_55			"bmp/map.bmp"
# define BMP_56			"bmp/map_dark.bmp"
# define BMP_57			"bmp/marble1.bmp"
# define BMP_58			"bmp/marble1_dark.bmp"
# define BMP_59			"bmp/marble2.bmp"
# define BMP_60			"bmp/marble2_dark.bmp"
# define BMP_61			"bmp/marbleflag.bmp"
# define BMP_62			"bmp/marbleflag_dark.bmp"
# define BMP_63			"bmp/metal.bmp"
# define BMP_64			"bmp/metal_dark.bmp"
# define BMP_65			"bmp/metaldoor1.bmp"
# define BMP_66			"bmp/metaldoor1_dark.bmp"
# define BMP_67			"bmp/metaldoor2.bmp"
# define BMP_68			"bmp/metaldoor2_dark.bmp"
# define BMP_69			"bmp/mossy.bmp"
# define BMP_70			"bmp/mossy_dark.bmp"
# define BMP_71			"bmp/purplestone2.bmp"
# define BMP_72			"bmp/purplestone2_dark.bmp"
# define BMP_73			"bmp/redbrick.bmp"
# define BMP_74			"bmp/redbrick_dark.bmp"
# define BMP_75			"bmp/rock1.bmp"
# define BMP_76			"bmp/rock1_dark.bmp"
# define BMP_77			"bmp/rock2.bmp"
# define BMP_78			"bmp/rock2_dark.bmp"
# define BMP_79			"bmp/rock3.bmp"
# define BMP_80			"bmp/rock3_dark.bmp"
# define BMP_81			"bmp/rock4.bmp"
# define BMP_82			"bmp/rock4_dark.bmp"
# define BMP_83			"bmp/stonepainting.bmp"
# define BMP_84			"bmp/stonepainting_dark.bmp"
# define BMP_85			"bmp/Swastika.bmp"
# define BMP_86			"bmp/swatiska_dark.bmp"
# define BMP_87			"bmp/verboten.bmp"
# define BMP_88			"bmp/verboten_dark.bmp"
# define BMP_89			"bmp/verboten2.bmp"
# define BMP_90			"bmp/verboten2_dark.bmp"
# define BMP_91			"bmp/wolf3d.bmp"

# define SPRITE_0		"sprites/aardwolf.bmp"
# define SPRITE_1		"sprites/ammo.bmp"
# define SPRITE_2		"sprites/armor.bmp"
# define SPRITE_3		"sprites/balloon.bmp"
# define SPRITE_4		"sprites/barrel.bmp"
# define SPRITE_5		"sprites/bed.bmp"
# define SPRITE_6		"sprites/big_gun.bmp"
# define SPRITE_7		"sprites/blood.bmp"
# define SPRITE_8		"sprites/blue_key.bmp"
# define SPRITE_9		"sprites/bones.bmp"
# define SPRITE_10		"sprites/chalice.bmp"
# define SPRITE_11		"sprites/chandelier.bmp"
# define SPRITE_12		"sprites/chicken_leg.bmp"
# define SPRITE_13		"sprites/cross.bmp"
# define SPRITE_14		"sprites/crown.bmp"
# define SPRITE_15		"sprites/crushed_bones.bmp"
# define SPRITE_16		"sprites/crushed_human.bmp"
# define SPRITE_17		"sprites/dog_food.bmp"
# define SPRITE_18		"sprites/empty_hanged_cage.bmp"
# define SPRITE_19		"sprites/empty_pot.bmp"
# define SPRITE_20		"sprites/empty_well.bmp"
# define SPRITE_21		"sprites/fastfood_table.bmp"
# define SPRITE_22		"sprites/flag.bmp"
# define SPRITE_23		"sprites/golden_key.bmp"
# define SPRITE_24		"sprites/green_barrel.bmp"
# define SPRITE_25		"sprites/green_lamp.bmp"
# define SPRITE_26		"sprites/gun.bmp"
# define SPRITE_27		"sprites/hanged_skeleton.bmp"
# define SPRITE_28		"sprites/hard_crushed_bones.bmp"
# define SPRITE_29		"sprites/jewels.bmp"
# define SPRITE_30		"sprites/lamp.bmp"
# define SPRITE_31		"sprites/lot_of_pans.bmp"
# define SPRITE_32		"sprites/med_kit.bmp"
# define SPRITE_33		"sprites/pans.bmp"
# define SPRITE_34		"sprites/pillar.bmp"
# define SPRITE_35		"sprites/plants.bmp"
# define SPRITE_36		"sprites/pot.bmp"
# define SPRITE_37		"sprites/rack.bmp"
# define SPRITE_38		"sprites/sink.bmp"
# define SPRITE_39		"sprites/skeleton_cage.bmp"
# define SPRITE_40		"sprites/sleepy_skeleton.bmp"
# define SPRITE_41		"sprites/stove.bmp"
# define SPRITE_42		"sprites/table.bmp"
# define SPRITE_43		"sprites/trash.bmp"
# define SPRITE_44		"sprites/tree.bmp"
# define SPRITE_45		"sprites/vase.bmp"
# define SPRITE_46		"sprites/water.bmp"
# define SPRITE_47		"sprites/well.bmp"

# define ANIM_0_0		"anim/carabin_0.bmp"
# define ANIM_0_1		"anim/carabin_1.bmp"
# define ANIM_0_2		"anim/carabin_2.bmp"
# define ANIM_0_3		"anim/carabin_3.bmp"
# define ANIM_0_4		"anim/carabin_4.bmp"
# define ANIM_1_0		"anim/gatling_0.bmp"
# define ANIM_1_1		"anim/gatling_1.bmp"
# define ANIM_1_2		"anim/gatling_2.bmp"
# define ANIM_1_3		"anim/gatling_3.bmp"
# define ANIM_1_4		"anim/gatling_4.bmp"
# define ANIM_2_0		"anim/gun_0.bmp"
# define ANIM_2_1		"anim/gun_1.bmp"
# define ANIM_2_2		"anim/gun_2.bmp"
# define ANIM_2_3		"anim/gun_3.bmp"
# define ANIM_2_4		"anim/gun_4.bmp"
# define ANIM_3_0		"anim/knife_0.bmp"
# define ANIM_3_1		"anim/knife_1.bmp"
# define ANIM_3_2		"anim/knife_2.bmp"
# define ANIM_3_3		"anim/knife_3.bmp"
# define ANIM_3_4		"anim/knife_4.bmp"

# define HITLER_0_1		"bmp/hitler/hitler_0_1.bmp"
# define HITLER_0_2		"bmp/hitler/hitler_0_2.bmp"
# define HITLER_0_3		"bmp/hitler/hitler_0_3.bmp"
# define HITLER_0_4		"bmp/hitler/hitler_0_4.bmp"
# define HITLER_1_0		"bmp/hitler/hitler_1_0.bmp"
# define HITLER_1_1		"bmp/hitler/hitler_1_1.bmp"
# define HITLER_1_2		"bmp/hitler/hitler_1_2.bmp"
# define HITLER_1_3		"bmp/hitler/hitler_1_3.bmp"
# define HITLER_1_4		"bmp/hitler/hitler_1_4.bmp"
# define HITLER_1_5		"bmp/hitler/hitler_1_5.bmp"
# define HITLER_1_6		"bmp/hitler/hitler_1_6.bmp"
# define HITLER_2_0		"bmp/hitler/hitler_2_0.bmp"
# define HITLER_2_1		"bmp/hitler/hitler_2_1.bmp"
# define HITLER_2_2		"bmp/hitler/hitler_2_2.bmp"
# define HITLER_2_3		"bmp/hitler/hitler_2_3.bmp"
# define HITLER_2_4		"bmp/hitler/hitler_2_4.bmp"
# define HITLER_2_5		"bmp/hitler/hitler_2_5.bmp"
# define HITLER_2_6		"bmp/hitler/hitler_2_6.bmp"
# define HITLER_3_0		"bmp/hitler/hitler_3_0.bmp"
# define HITLER_3_1		"bmp/hitler/hitler_3_1.bmp"
# define HITLER_3_2		"bmp/hitler/hitler_3_2.bmp"
# define HITLER_3_3		"bmp/hitler/hitler_3_3.bmp"
# define HITLER_4_0		"bmp/hitler/hitler_4_0.bmp"
# define HITLER_4_1		"bmp/hitler/hitler_4_1.bmp"
# define HITLER_4_2		"bmp/hitler/hitler_4_2.bmp"
# define HITLER_4_3		"bmp/hitler/hitler_4_3.bmp"
# define HITLER_4_4		"bmp/hitler/hitler_4_4.bmp"
# define HITLER_4_5		"bmp/hitler/hitler_4_5.bmp"
# define HITLER_4_6		"bmp/hitler/hitler_4_6.bmp"
# define HITLER_5_0		"bmp/hitler/hitler_5_0.bmp"
# define HITLER_5_1		"bmp/hitler/hitler_5_1.bmp"
# define HITLER_5_2		"bmp/hitler/hitler_5_2.bmp"
# define HITLER_5_3		"bmp/hitler/hitler_5_3.bmp"
# define HITLER_5_4		"bmp/hitler/hitler_5_4.bmp"
# define HITLER_5_5		"bmp/hitler/hitler_5_5.bmp"
# define HITLER_5_6		"bmp/hitler/hitler_5_6.bmp"
# define HITLER_5_7		"bmp/hitler/hitler_5_7.bmp"

# define GAMEOVER		"bmp/GameOver.bmp"

#endif
