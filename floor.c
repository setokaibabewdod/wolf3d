/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floor.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 10:53:33 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			draw_floor_sky_2(t_var *var, t_raycast_pack rp,
		t_floor_pack fp)
{
	int				y;
	t_bmp			*bmp;

	bmp = var->tab_bmp[41];
	y = rp.draw_end;
	while (++y < HEIGHT)
	{
		fp.current_dist = HEIGHT / (2.0 * y - HEIGHT);
		fp.weight = (fp.current_dist - fp.dist_player) /
				(fp.dist_wall - fp.dist_player);
		fp.current_floor_x = fp.weight * fp.floor_x_wall +
				(1.0 - fp.weight) * var->player->pos.x;
		fp.current_floor_y = fp.weight * fp.floor_y_wall +
				(1.0 - fp.weight) * var->player->pos.y;
		fp.floor_tex_x = (int)(fp.current_floor_x * 64) % 64;
		fp.floor_tex_y = (int)(fp.current_floor_y * 64) % 64;
		fp.off_height = rp.line_height / 2 - HEIGHT / 2;
		var->img_str[rp.x + y * LENGTH] = get_color(bmp, fp.floor_tex_x,
				fp.floor_tex_y);
	}
}

static void			draw_floor_sky3(t_raycast_pack rp, t_floor_pack *fp)
{
	fp->floor_x_wall = rp.map_x;
	fp->floor_y_wall = rp.map_y + rp.wall_x;
}

void				draw_floor_sky(t_var *var, t_raycast_pack rp)
{
	t_floor_pack	fp;

	if (rp.side == 0 && rp.ray_dir_x > 0)
		draw_floor_sky3(rp, &fp);
	else if (rp.side == 0 && rp.ray_dir_x < 0)
	{
		fp.floor_x_wall = rp.map_x + 1.0;
		fp.floor_y_wall = rp.map_y + rp.wall_x;
	}
	else if (rp.side == 1 && rp.ray_dir_y > 0)
	{
		fp.floor_x_wall = rp.map_x + rp.wall_x;
		fp.floor_y_wall = rp.map_y;
	}
	else
	{
		fp.floor_x_wall = rp.map_x + rp.wall_x;
		fp.floor_y_wall = rp.map_y + 1.0;
	}
	fp.dist_wall = rp.perp_wall_dist;
	fp.dist_player = 0.0;
	if (rp.draw_end < 0)
		rp.draw_end = HEIGHT;
	draw_floor_sky_2(var, rp, fp);
}
