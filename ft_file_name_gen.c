/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file_name_gen.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 11:51:22 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_file_name_gen(char **name)
{
	int		nb;
	char	*nb_s;
	int		fd;

	nb = 0;
	nb_s = ft_itoa(nb);
	nb_s = ft_strjoin_free(*name, nb_s, 2);
	while ((fd = open(nb_s, O_RDONLY)) != -1)
	{
		nb++;
		nb_s = ft_itoa(nb);
		nb_s = ft_strjoin_free(*name, nb_s, 2);
		close(fd);
	}
	*name = ft_strdup(nb_s);
	return (1);
}
