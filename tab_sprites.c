/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_sprites.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 15:45:47 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			init_tab_sprite_3(t_var *var)
{
	var->tab_sprite[41] = new_bmp(SPRITE_41);
	var->tab_sprite[42] = new_bmp(SPRITE_42);
	var->tab_sprite[43] = new_bmp(SPRITE_43);
	var->tab_sprite[44] = new_bmp(SPRITE_44);
	var->tab_sprite[45] = new_bmp(SPRITE_45);
	var->tab_sprite[46] = new_bmp(SPRITE_46);
	var->tab_sprite[47] = new_bmp(SPRITE_47);
	init_tab_enemies(var);
}

static void			init_tab_sprite_2(t_var *var)
{
	var->tab_sprite[21] = new_bmp(SPRITE_21);
	var->tab_sprite[22] = new_bmp(SPRITE_22);
	var->tab_sprite[23] = new_bmp(SPRITE_23);
	var->tab_sprite[24] = new_bmp(SPRITE_24);
	var->tab_sprite[25] = new_bmp(SPRITE_25);
	var->tab_sprite[26] = new_bmp(SPRITE_26);
	var->tab_sprite[27] = new_bmp(SPRITE_27);
	var->tab_sprite[28] = new_bmp(SPRITE_28);
	var->tab_sprite[29] = new_bmp(SPRITE_29);
	var->tab_sprite[30] = new_bmp(SPRITE_30);
	var->tab_sprite[31] = new_bmp(SPRITE_31);
	var->tab_sprite[32] = new_bmp(SPRITE_32);
	var->tab_sprite[33] = new_bmp(SPRITE_33);
	var->tab_sprite[34] = new_bmp(SPRITE_34);
	var->tab_sprite[35] = new_bmp(SPRITE_35);
	var->tab_sprite[36] = new_bmp(SPRITE_36);
	var->tab_sprite[37] = new_bmp(SPRITE_37);
	var->tab_sprite[38] = new_bmp(SPRITE_38);
	var->tab_sprite[39] = new_bmp(SPRITE_39);
	var->tab_sprite[40] = new_bmp(SPRITE_40);
	init_tab_sprite_3(var);
}

void				init_tab_sprite(t_var *var)
{
	var->tab_sprite[0] = new_bmp(SPRITE_0);
	var->tab_sprite[1] = new_bmp(SPRITE_1);
	var->tab_sprite[2] = new_bmp(SPRITE_2);
	var->tab_sprite[3] = new_bmp(SPRITE_3);
	var->tab_sprite[4] = new_bmp(SPRITE_4);
	var->tab_sprite[5] = new_bmp(SPRITE_5);
	var->tab_sprite[6] = new_bmp(SPRITE_6);
	var->tab_sprite[7] = new_bmp(SPRITE_7);
	var->tab_sprite[8] = new_bmp(SPRITE_8);
	var->tab_sprite[9] = new_bmp(SPRITE_9);
	var->tab_sprite[10] = new_bmp(SPRITE_10);
	var->tab_sprite[11] = new_bmp(SPRITE_11);
	var->tab_sprite[12] = new_bmp(SPRITE_12);
	var->tab_sprite[13] = new_bmp(SPRITE_13);
	var->tab_sprite[14] = new_bmp(SPRITE_14);
	var->tab_sprite[15] = new_bmp(SPRITE_15);
	var->tab_sprite[16] = new_bmp(SPRITE_16);
	var->tab_sprite[17] = new_bmp(SPRITE_17);
	var->tab_sprite[18] = new_bmp(SPRITE_18);
	var->tab_sprite[19] = new_bmp(SPRITE_19);
	var->tab_sprite[20] = new_bmp(SPRITE_20);
	init_tab_sprite_2(var);
}

static void			init_tab_enemies_2(t_var *var)
{
	var->tab_sprite[69] = new_bmp(HITLER_3_3);
	var->tab_sprite[EN_NORMIE] = new_bmp(HITLER_4_0);
	var->tab_sprite[71] = new_bmp(HITLER_4_1);
	var->tab_sprite[72] = new_bmp(HITLER_4_2);
	var->tab_sprite[73] = new_bmp(HITLER_4_3);
	var->tab_sprite[74] = new_bmp(HITLER_4_4);
	var->tab_sprite[75] = new_bmp(HITLER_4_5);
	var->tab_sprite[76] = new_bmp(HITLER_4_6);
	var->tab_sprite[EN_NORMIE_DEAD] = new_bmp(HITLER_5_0);
	var->tab_sprite[78] = new_bmp(HITLER_5_1);
	var->tab_sprite[79] = new_bmp(HITLER_5_2);
	var->tab_sprite[80] = new_bmp(HITLER_5_3);
	var->tab_sprite[81] = new_bmp(HITLER_5_4);
	var->tab_sprite[82] = new_bmp(HITLER_5_5);
	var->tab_sprite[83] = new_bmp(HITLER_5_6);
	var->tab_sprite[84] = new_bmp(HITLER_5_7);
	var->tab_sprite[85] = new_bmp(GAMEOVER);
}

void				init_tab_enemies(t_var *var)
{
	var->tab_sprite[EN_PRIEST] = new_bmp(HITLER_0_1);
	var->tab_sprite[49] = new_bmp(HITLER_0_2);
	var->tab_sprite[50] = new_bmp(HITLER_0_3);
	var->tab_sprite[51] = new_bmp(HITLER_0_4);
	var->tab_sprite[EN_PRIEST_DEAD] = new_bmp(HITLER_1_0);
	var->tab_sprite[53] = new_bmp(HITLER_1_1);
	var->tab_sprite[54] = new_bmp(HITLER_1_2);
	var->tab_sprite[55] = new_bmp(HITLER_1_3);
	var->tab_sprite[56] = new_bmp(HITLER_1_4);
	var->tab_sprite[57] = new_bmp(HITLER_1_5);
	var->tab_sprite[58] = new_bmp(HITLER_1_6);
	var->tab_sprite[EN_MECH] = new_bmp(HITLER_2_0);
	var->tab_sprite[60] = new_bmp(HITLER_2_1);
	var->tab_sprite[61] = new_bmp(HITLER_2_2);
	var->tab_sprite[62] = new_bmp(HITLER_2_3);
	var->tab_sprite[63] = new_bmp(HITLER_2_4);
	var->tab_sprite[64] = new_bmp(HITLER_2_5);
	var->tab_sprite[65] = new_bmp(HITLER_2_6);
	var->tab_sprite[EN_MECH_DEAD] = new_bmp(HITLER_3_0);
	var->tab_sprite[67] = new_bmp(HITLER_3_1);
	var->tab_sprite[68] = new_bmp(HITLER_3_2);
	init_tab_enemies_2(var);
}
