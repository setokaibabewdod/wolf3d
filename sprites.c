/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprites.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 10:50:33 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void				sort_sprites(t_order *tab_order, t_var *var)
{
	int				gap;
	int				swapped;
	int				i;
	int				j;

	swapped = 0;
	gap = var->num_sprites;
	while (gap > 1 || swapped)
	{
		swapped = 0;
		gap = (gap * 10) / 13;
		if (gap == 9 || gap == 10)
			gap = 11;
		if (gap < 1)
			gap = 1;
		i = 0;
		while (i < var->num_sprites - gap)
		{
			j = i + gap;
			if (tab_order[i].distance < tab_order[j].distance)
				swapped = double_swap(tab_order, i, j);
			i++;
		}
	}
}

static void			draw_sprites4(t_var *var, t_sprite_pack sp, int y,
		t_bmp *bmp_sprite)
{
	int				color;

	color = get_color(bmp_sprite, sp.tex_x,
			(((int)(y * 256 - HEIGHT * 128 + sp.sprite_height * 128) *
			bmp_sprite->header.width_px) / sp.sprite_height) / 256);
	if (color != PINK)
		var->img_str[sp.stripe + y * LENGTH] = color;
}

static void			draw_sprites3(t_var *var, t_order *tab_order,
		double *z_buffer, t_sprite_pack sp)
{
	t_bmp			*bmp_sprite;
	int				y;

	bmp_sprite = var->tab_sprite[var->sprites[tab_order[sp.i].order].texture];
	if (var->sprites[tab_order[sp.i].order].texture != -1)
	{
		while (sp.stripe <= sp.draw_end_x)
		{
			sp.tex_x = (int)((sp.stripe -
					(-sp.sprite_width / 2 + sp.sprite_screen_x)) *
							bmp_sprite->header.width_px / sp.sprite_width);
			if (sp.transform_y >= 0 && sp.stripe >= 0 && sp.stripe < LENGTH &&
					sp.transform_y < z_buffer[sp.stripe])
			{
				y = sp.draw_start_y - 1;
				if (sp.stripe == (LENGTH - 1) / 2 && var->player->is_shooting &&
						var->sprites[tab_order[sp.i].order].is_enemy)
					shoot_on_enemy(&(var->sprites[tab_order[sp.i].order]));
				while (++y < sp.draw_end_y)
					draw_sprites4(var, sp, y, bmp_sprite);
			}
			sp.stripe++;
		}
	}
}

static void			draw_sprites2(t_var *var, int i, t_order *tab_order,
		double *z_buffer)
{
	t_sprite_pack	sp;

	sp.i = i;
	sp.sprite_x = var->sprites[tab_order[sp.i].order].x - var->player->pos.x;
	sp.sprite_y = var->sprites[tab_order[sp.i].order].y - var->player->pos.y;
	sp.transform_x = (sp.sprite_x * var->player->dir.y - var->player->dir.x *
			sp.sprite_y) / (var->player->plane.x * var->player->dir.y -
			var->player->dir.x * var->player->plane.y);
	sp.transform_y = (-var->player->plane.y * sp.sprite_x +
			var->player->plane.x * sp.sprite_y) / (var->player->plane.x *
			var->player->dir.y - var->player->dir.x * var->player->plane.y);
	sp.sprite_screen_x = (int)((LENGTH / 2) *
			(1 + sp.transform_x / sp.transform_y));
	sp.sprite_height = abs((int)(HEIGHT / (sp.transform_y)));
	if ((sp.draw_start_y = -sp.sprite_height / 2 + HEIGHT / 2) < 0)
		sp.draw_start_y = 0;
	if ((sp.draw_end_y = sp.sprite_height / 2 + HEIGHT / 2) >= HEIGHT)
		sp.draw_end_y = HEIGHT;
	sp.sprite_width = abs((int)(HEIGHT / (sp.transform_y)));
	sp.stripe = ((-sp.sprite_width / 2 + sp.sprite_screen_x) >= 0) ?
			(-sp.sprite_width / 2 + sp.sprite_screen_x) : 0;
	sp.draw_end_x = sp.sprite_width / 2 + sp.sprite_screen_x;
	if (sp.draw_end_x >= LENGTH)
		sp.draw_end_x = LENGTH - 1;
	draw_sprites3(var, tab_order, z_buffer, sp);
}

int					draw_sprites(t_var *var, double *z_buffer)
{
	int				i;
	t_order			*tab_order;

	if (!(tab_order = malloc(sizeof(t_order) * var->num_sprites)))
		return (error_sprites(var->sprites, tab_order));
	i = -1;
	while (++i < var->num_sprites)
	{
		tab_order[i].order = i;
		tab_order[i].distance = ((var->player->pos.x - var->sprites[i].x) *
				(var->player->pos.x - var->sprites[i].x) +
				(var->player->pos.y - var->sprites[i].y) *
				(var->player->pos.y - var->sprites[i].y));
	}
	i = -1;
	sort_sprites(tab_order, var);
	while (++i < var->num_sprites)
		draw_sprites2(var, i, tab_order, z_buffer);
	free(tab_order);
	return (1);
}
