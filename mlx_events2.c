/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_events2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <cvan-duf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 11:19:03 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void			ft_expose_hook2(t_var *var)
{
	if (!var->gameover)
	{
		if (!(mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->img_ptr,
				420, 0)))
			ft_error(var);
		if (!(mlx_put_image_to_window(var->mlx_ptr, var->win_ptr, var->hud_ptr,
				420, HEIGHT)))
			ft_error(var);
	}
	else
	{
		draw_gameover(var);
		if (!(mlx_put_image_to_window(var->mlx_ptr, var->win_ptr,
				var->go_ptr, 420, 0)))
			ft_error(var);
	}
}

int					ft_expose_hook(t_var *var)
{
	if ((int)var->player->pos.x == 68 && (int)var->player->pos.y == 21)
	{
		var->player->pos.x = 20;
		var->player->pos.y = 20;
	}
	if (var->img_ptr != NULL)
		mlx_destroy_image(var->mlx_ptr, var->img_ptr);
	if (!(var->img_ptr = mlx_new_image(var->mlx_ptr, LENGTH, HEIGHT)))
		ft_error(var);
	var->img_str = (int *)mlx_get_data_addr(var->img_ptr,
			&(var->img->bits), &(var->img->length), &(var->img->endian));
	if (var->map_editor == 1)
		map_editor(var);
	ray_cast(var);
	if (var->draw_hud == 1)
	{
		draw_hud(var);
		var->draw_hud = 0;
	}
	minimap(var);
	ft_expose_hook2(var);
	return (0);
}
