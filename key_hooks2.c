/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hooks2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jreynaer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 13:52:03 by jreynaer          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	map_editor1(int key, t_var *var)
{
	if (key == C_KEY && var->map_editor == 2)
	{
		if (var->lock == 0)
		{
			var->lock_val = var->map[var->mini_y][var->mini_x];
			var->lock = 1;
		}
		else
			var->lock = 0;
	}
	else if (key == NK0_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 <= 91)
			var->map[var->mini_y][var->mini_x] *= 10;
	}
}

static void	map_editor2(int key, t_var *var)
{
	if (key == NK1_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 1 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 1;
		}
	}
	else if (key == NK2_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 2 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 2;
		}
	}
	else if (key == NK3_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 3 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 3;
		}
	}
}

static void	map_editor3(int key, t_var *var)
{
	if (key == NK4_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 4 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 4;
		}
	}
	else if (key == NK5_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 5 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 5;
		}
	}
	else if (key == NK6_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 6 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 6;
		}
	}
}

static void	map_editor4(int key, t_var *var)
{
	if (key == NK7_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 7 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 7;
		}
	}
	else if (key == NK8_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 8 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 8;
		}
	}
	else if (key == NK9_KEY && var->map_editor == 2)
	{
		if (var->map[var->mini_y][var->mini_x] * 10 + 9 <= 91)
		{
			var->map[var->mini_y][var->mini_x] *= 10;
			var->map[var->mini_y][var->mini_x] += 9;
		}
	}
}

void		key_hooks2(int key, t_var *var)
{
	map_editor1(key, var);
	if (var->mini_x != (int)var->player->pos.x ||
			var->mini_y != (int)var->player->pos.y)
	{
		map_editor2(key, var);
		map_editor3(key, var);
		map_editor4(key, var);
	}
}
