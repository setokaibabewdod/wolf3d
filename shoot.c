/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shoot.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 11:46:43 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			shoot_on_enemy(t_sprite *enemy)
{
	if (!enemy->is_dead)
	{
		enemy->moving = 0;
		if (!enemy->is_dead && enemy->base_tex == EN_NORMIE)
		{
			enemy->base_tex = EN_NORMIE_DEAD;
			enemy->texture = EN_NORMIE_DEAD;
			enemy->nb_anim = 8;
		}
		else if (!enemy->is_dead && enemy->base_tex == EN_PRIEST)
		{
			enemy->base_tex = EN_PRIEST_DEAD;
			enemy->texture = EN_PRIEST_DEAD;
			enemy->nb_anim = 7;
		}
		else if (!enemy->is_dead && enemy->base_tex == EN_MECH)
		{
			enemy->base_tex = EN_MECH_DEAD;
			enemy->texture = EN_MECH_DEAD;
			enemy->nb_anim = 4;
		}
	}
}

void			dead_enemy_texture(t_sprite *enemy)
{
	enemy->is_dead = 1;
	if (enemy->base_tex == EN_NORMIE_DEAD)
		enemy->texture = EN_NORMIE_DEAD + enemy->nb_anim - 1;
	else if (enemy->base_tex == EN_PRIEST_DEAD)
		enemy->texture = EN_PRIEST_DEAD + enemy->nb_anim - 1;
	else if (enemy->base_tex == EN_MECH_DEAD)
		enemy->texture = EN_MECH_DEAD + enemy->nb_anim - 1;
}
