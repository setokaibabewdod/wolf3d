/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 12:02:51 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_player		*new_player(t_var *var)
{
	t_player	*player;

	if (!(player = (t_player *)ft_memalloc(sizeof(t_player))))
		ft_error(NULL);
	new_inventory(player);
	check_pos(var);
	player->weapon_anim = 10;
	player->life_total = PLAYER_LIFE_TOTAL;
	player->life = PLAYER_LIFE_TOTAL;
	player->weapons[0] = -1;
	player->weapons[1] = 10;
	player->weapons[2] = -1;
	player->weapons[3] = -1;
	player->is_shooting = 0;
	return (player);
}

int				check_pos(t_var *var)
{
	int			i;
	int			j;

	j = 0;
	while (j < var->map_y)
	{
		i = 0;
		while (i < var->map_x)
		{
			if (var->map[j][i] == 0)
			{
				var->player->pos.x = (float)i + 0.5;
				var->player->pos.y = (float)j + 0.5;
				return (1);
			}
			i++;
		}
		j++;
	}
	return (0);
}
