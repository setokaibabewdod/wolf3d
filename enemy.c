/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemy.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 10:55:12 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int				check_enemy_on_map_x_2(t_var *var, int i,
		double enemy_speed)
{
	int j;

	j = 0;
	if ((int)(var->sprites[i].x - enemy_speed) == (int)(var->sprites[i].x))
		return (1);
	while (j < var->num_sprites)
	{
		if ((int)(var->sprites[i].x - enemy_speed) ==
				(int)(var->sprites[j].x) &&
				(int)(var->sprites[i].y) == (int)(var->sprites[j].y))
			return (-1);
		j++;
	}
	return (1);
}

int						check_enemy_on_map_x(t_var *var, float dx, int i)
{
	if (dx > 0)
		return (check_enemy_on_map_x_2(var, i, ENEMY_SPEED));
	return (check_enemy_on_map_x_2(var, i, -ENEMY_SPEED));
}

static int				check_enemy_on_map_y_2(t_var *var, int i,
		double enemy_speed)
{
	int j;

	j = 0;
	if ((int)(var->sprites[i].y - enemy_speed) == (int)(var->sprites[i].y))
		return (1);
	while (j < var->num_sprites)
	{
		if (var->sprites[j].is_enemy &&
				(int)(var->sprites[i].y - enemy_speed) ==
				(int)(var->sprites[j].y) &&
				(int)(var->sprites[i].x) == (int)(var->sprites[j].x))
			return (-1);
		j++;
	}
	return (1);
}

int						check_enemy_on_map_y(t_var *var, float dy, int i)
{
	int					j;

	j = 0;
	if (dy > 0)
		return (check_enemy_on_map_y_2(var, i, ENEMY_SPEED));
	return (check_enemy_on_map_y_2(var, i, -ENEMY_SPEED));
}
