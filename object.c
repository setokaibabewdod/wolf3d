/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 12:07:06 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				check_object(t_var *var)
{
	int			i;
	int			texture;

	i = 0;
	while (i < var->num_sprites)
	{
		if ((int)var->player->pos.x == (int)var->sprites[i].x &&
				(int)var->player->pos.y == (int)var->sprites[i].y &&
				var->sprites[i].texture != -1 && var->sprites[i].lootable)
		{
			texture = (int)var->sprites[i].texture;
			return (texture);
		}
		i++;
	}
	return (-1);
}

void			remove_object(t_var *var)
{
	int			i;

	i = 0;
	while (i < var->num_sprites)
	{
		if ((int)var->player->pos.x == (int)var->sprites[i].x &&
				(int)var->player->pos.y == (int)var->sprites[i].y &&
				var->sprites[i].texture != -1 && var->sprites[i].lootable)
		{
			var->sprites[i].texture = -1;
			return ;
		}
		i++;
	}
}
