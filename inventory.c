/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inventory.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:13:16 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/24 10:16:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				player_has_obj(t_player *player, int object)
{
	int			i;

	i = 0;
	while (i < NUM_OBJ)
	{
		if (player->inventory[i] == object)
			return (1);
		i++;
	}
	return (0);
}

int				add_item_to_inventory(t_player *player, int object)
{
	int				i;

	if (object == -1)
		return (0);
	if (object == 6)
	{
		player->weapons[3] = 0;
		return (1);
	}
	if (object == 26)
	{
		player->weapons[2] = 5;
		return (1);
	}
	i = 0;
	while (i < NUM_OBJ && player->inventory[i] != -1)
		i++;
	if (i == NUM_OBJ)
		return (0);
	player->inventory[i] = object;
	player->inventory_empty = 0;
	return (1);
}

void			check_empty_inventory(t_player *player)
{
	int			i;

	i = 0;
	while (i < NUM_OBJ)
	{
		if (player->inventory[i] != -1)
		{
			player->inventory_empty = 0;
			return ;
		}
		i++;
	}
	player->inventory_empty = 1;
}

int				del_item_from_inventory(t_player *player, int object)
{
	int			i;

	i = 0;
	if (object == -1)
		return (0);
	while (i < NUM_OBJ && player->inventory[i] != object)
		i++;
	player->inventory[i] = -1;
	check_empty_inventory(player);
	return (1);
}
